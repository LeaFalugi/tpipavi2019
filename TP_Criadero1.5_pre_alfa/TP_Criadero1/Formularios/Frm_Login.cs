﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Login : Form
    {

        
        public Frm_Login()
        {
            InitializeComponent();
        }

        public String usuario
        {
            get { return txt_usuario.Text; }
            set { txt_usuario.Text = value; }
        }

        public String clave
        {
            get { return txt_contraseña.Text; }
            set { txt_contraseña.Text = value; }
        }

        public int _Dni { get; set; }


        private void txt_usuario_Enter(object sender, EventArgs e)
        {
            if (txt_usuario.Text == "Ingrese un usuario...")
            {
                txt_usuario.Text = "";
                txt_usuario.ForeColor = SystemColors.WindowText;
            }
        }

        private void txt_usuario_Leave(object sender, EventArgs e)
        {
            if (txt_usuario.Text == "")
            {
                txt_usuario.Text = "Ingrese un usuario...";
                txt_usuario.ForeColor = SystemColors.InactiveCaption;
            }
        }

        private void txt_contraseña_Enter(object sender, EventArgs e)
        {
            if (txt_contraseña.Text == "Ingrese una contraseña...")
            {


                txt_contraseña.Text = "";
                txt_contraseña.ForeColor = SystemColors.WindowText;
                txt_contraseña.PasswordChar = '*';

            }
        }

        private void txt_contraseña_Leave(object sender, EventArgs e)
        {
            if (txt_contraseña.Text == "")
            {
                txt_contraseña.PasswordChar = '\0';
                txt_contraseña.Text = "Ingrese una contraseña...";
                txt_contraseña.ForeColor = SystemColors.InactiveCaption;
            }
        }

        private void btn_ingresar_Click(object sender, EventArgs e)
        {

            if (this.txt_usuario.Text == "Ingrese un usuario...")
            {
                MessageBox.Show("Falta cargar usuario.", "Datos Incompletos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_usuario.Focus();
                return;
            }
            if (this.txt_contraseña.Text == "Ingrese una contraseña...")
            {
                MessageBox.Show("Falta cargar contraseña.", "Datos Incompletos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_contraseña.Focus();
                return;
            }

            NG_Users usuarios = new NG_Users();
            if (usuarios.validar_usuario(txt_usuario.Text, txt_contraseña.Text) == NG_Users.respuesta.correcto)
            {
                this._Dni = usuarios.recuperar_Dni(txt_usuario.Text, txt_contraseña.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("Los datos son incorrectos");
                this._Dni = 0;
            }
        }

        private void Frm_Login_Load(object sender, EventArgs e)
        {

        }
    }
}
