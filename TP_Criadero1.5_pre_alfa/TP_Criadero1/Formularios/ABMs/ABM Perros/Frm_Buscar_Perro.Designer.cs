﻿namespace TP_Criadero1
{
    partial class Frm_Buscar_Perro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Buscar_Perro));
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.Usuario = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.txt_id_perro = new System.Windows.Forms.TextBox();
            this.Usuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(255, 151);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 1;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // Usuario
            // 
            this.Usuario.Controls.Add(this.label2);
            this.Usuario.Controls.Add(this.btn_buscar);
            this.Usuario.Controls.Add(this.txt_id_perro);
            this.Usuario.Location = new System.Drawing.Point(12, 22);
            this.Usuario.Name = "Usuario";
            this.Usuario.Size = new System.Drawing.Size(295, 112);
            this.Usuario.TabIndex = 0;
            this.Usuario.TabStop = false;
            this.Usuario.Text = "Perro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ID Perro";
            // 
            // btn_buscar
            // 
            this.btn_buscar.Location = new System.Drawing.Point(209, 71);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 1;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.Btn_buscar_Click);
            // 
            // txt_id_perro
            // 
            this.txt_id_perro.Location = new System.Drawing.Point(97, 35);
            this.txt_id_perro.Name = "txt_id_perro";
            this.txt_id_perro.Size = new System.Drawing.Size(148, 20);
            this.txt_id_perro.TabIndex = 0;
            // 
            // Frm_Buscar_Perro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(339, 185);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.Usuario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_Buscar_Perro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscar Perro";
            this.Load += new System.EventHandler(this.Frm_Buscar_Perro_Load);
            this.Usuario.ResumeLayout(false);
            this.Usuario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.GroupBox Usuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox txt_id_perro;
    }
}