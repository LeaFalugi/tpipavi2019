﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;


namespace TP_Criadero1
{
    public partial class Frm_Modificar_Perro : Form
    {
        public string id { get; set; }
        public int Sexo(string _sexo)
        {
            switch (_sexo)
            {
                case "Macho":
                    return 0;
                case "Hembra":
                    return 1;
                default:
                    return -1;
            }
        }

        public string sexo()
        {
            if (cmb_sexo.SelectedIndex==0)
            {
                return "Macho";
            }
            else
            {
                return "Hembra";
            }
        }
        private void recuperar_datos()
        {
            NG_perros ng_prro = new NG_perros();
            DataTable tabla = new DataTable();

            tabla = ng_prro.recuperar_datos(id);

            txt_id_perro.Text = id;
            txt_nombre.Text = tabla.Rows[0]["nombre"].ToString();
            cmb_sexo.SelectedIndex = Sexo(tabla.Rows[0]["sexo"].ToString());

            if (tabla.Rows[0]["idPerroMadre"].ToString() != "")
            {
                txt_madre.Text = ng_prro.tomar_nombre_madre(tabla.Rows[0]["idPerroMadre"].ToString());
            }

            if (tabla.Rows[0]["fechanacimiento"].ToString()!="")
            {
                dtp_fecha_nac.Value = DateTime.Parse(tabla.Rows[0]["fechanacimiento"].ToString());
            }

        }
        public Frm_Modificar_Perro()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Modificar_Perro_Load(object sender, EventArgs e)
        {
            recuperar_datos();
        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();
            NG_perros ng_prro = new NG_perros();

            if (txt_nombre.Text == "")
            {
                MessageBox.Show("El nombre esta vacio, por las dudas revise el resto de datos.");
                txt_nombre.Focus();
            }
            else
            {
                string fecha = date.convert_fecha(dtp_fecha_nac.Value);

                ng_prro.modificar_perro(id, txt_nombre.Text, txt_madre.Text,
                    fecha, sexo());

                MessageBox.Show("Datos modificados con exito.");
                this.Close();
            }
        }
    }
}
