﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Registrar_Perro : Form
    {
        public string sexo { get; set; }

        private void validar_sexo()
        {
            if (cmb_sexo.SelectedIndex == 0)
            {
                sexo = "Macho";
            }
            else
            {
                sexo = "Hembra";
            }
        }
        public Frm_Registrar_Perro()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Registrar_Perro_Load(object sender, EventArgs e)
        {
            
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            NG_perros perro = new NG_perros();
            BE_fecha date = new BE_fecha();

            if(txt_nombre.Text==""
                || cmb_sexo.SelectedIndex == -1)
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nombre.Focus();
                return;
            }
            else
            {
                string fecha = date.convert_fecha(dtp_fecha_nac.Value);
                validar_sexo();

                perro.registrar_perro(txt_nombre.Text, fecha, sexo, txt_madre.Text);

                MessageBox.Show("Se registro el perro con exito!.");
                this.Close();
            }


        }
    }
}
