﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Perros : Form
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_perros = new DataTable();

        private void cargar_grilla(DataTable tabla)
        {
            dgv_perros.Rows.Clear();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                
                
                dgv_perros.Rows.Add();

                dgv_perros.Rows[i].Cells[0].Value = tabla.Rows[i]["IdPerro"].ToString();
                dgv_perros.Rows[i].Cells[1].Value = tabla.Rows[i]["nombre"].ToString();
                dgv_perros.Rows[i].Cells[2].Value = tabla.Rows[i]["sexo"].ToString();
                dgv_perros.Rows[i].Cells[3].Value = tabla.Rows[i]["fechanacimiento"].ToString();
                dgv_perros.Rows[i].Cells[4].Value = tabla.Rows[i]["madre"].ToString();
               
            }
        }
        private void actualizar_grilla()
        {
            tabla_perros = bd.ejecutar_consulta("exec grilla_perros");
            cargar_grilla(tabla_perros);
        }
        public Frm_ABM_Perros()
        {
            InitializeComponent();
        }

        private void Frm_ABM_Perros_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Perro buscar_Perro = new Frm_Buscar_Perro();
            buscar_Perro.ShowDialog();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Perro modificar_Perro = new Frm_Modificar_Perro();

            modificar_Perro.id = dgv_perros.CurrentRow.Cells[0].Value.ToString();
            modificar_Perro.ShowDialog();
            actualizar_grilla();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Perro registrar_Perro = new Frm_Registrar_Perro();
            registrar_Perro.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_perros.CurrentRow == null)
            {
                return;
            }

            string IdperroSelec = dgv_perros.CurrentRow.Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea dar de baja al perro con ID: " +
                IdperroSelec + "? ", "Baja Perro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                Clases.Negocios.NG_perros NG_Perros = new Clases.Negocios.NG_perros();

                NG_Perros.baja_Perro(IdperroSelec);

                MessageBox.Show("Perro dado de baja", "Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            actualizar_grilla();
        }
    }
}
