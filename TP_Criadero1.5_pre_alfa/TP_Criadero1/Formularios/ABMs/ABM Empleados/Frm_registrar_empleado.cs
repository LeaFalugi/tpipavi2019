﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_registrar_empleado : Form
    {
        NG_empleado ng_emp = new NG_empleado();
        BE_Direcciones be_direccion = new BE_Direcciones();

        public string nombre{
            get { return txt_nombre.Text; }
            set { txt_nombre.Text = value; }
        }

        public string apellido
        {
            get { return txt_apellido.Text; }
            set { txt_apellido.Text = value; }
        }

        public int n_documento
        {
            get { return int.Parse(txt_n_documento.Text); }
            set { txt_n_documento.Text = value.ToString(); }
        }
        /*public int numeracion
        {
            get => int.Parse(txt_numeracion.Text);
            set => txt_numeracion.Text = value.ToString();
        }*/

        public Frm_registrar_empleado()
        {
            InitializeComponent();
        }

        private void Btn_registrar_Click_1(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();

            Console.WriteLine("Tipo Doc: index " + cmb_tipos_documento.SelectedIndex + " value " +
            cmb_tipos_documento.SelectedValue.ToString());
            Console.WriteLine("barrios: index " + cmb_barrio.SelectedIndex + " value " +
                cmb_barrio.SelectedValue.ToString());


            if (txt_nombre.Text == ""
                || txt_apellido.Text == ""
                || txt_n_documento.Text == ""
                || cmb_tipos_documento.SelectedIndex == -1
                )
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nombre.Focus();
            }
            else
            {
                string idDir = be_direccion.registrar_direccion(txt_calle.Text,
                    int.Parse(cmb_barrio.SelectedValue.ToString()), txt_numeracion.Text);

                string fecha = date.convert_fecha(dtp_fecha_nacimiento.Value);

                ng_emp.registrar_empleado(n_documento,cmb_tipos_documento.SelectedValue.ToString()
                    , nombre, apellido,fecha,txt_telefono.Text,idDir);

                MessageBox.Show("Se registro el nuevo empleado con exito.");
                this.Close();
                
            }
        }

        private void Btn_cancelar_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_registrar_empleado_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string cons_tipo = "Select * from tiposDni";

            tabla = bd.ejecutar_consulta(cons_tipo);

            cmb_tipos_documento.DataSource = tabla;
            cmb_tipos_documento.DisplayMember = "Descripcion";
            cmb_tipos_documento.ValueMember = "idtipodni";

            //cmb_tipos_documento.cargar();
            cmb_barrio.cargar();
        }

        private void Cmb_tipos_documento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
