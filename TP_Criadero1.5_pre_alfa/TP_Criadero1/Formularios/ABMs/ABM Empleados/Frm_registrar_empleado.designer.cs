﻿namespace TP_Criadero1
{
    partial class Frm_registrar_empleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_registrar_empleado));
            this.gpb_empleado = new System.Windows.Forms.GroupBox();
            this.btn_registrar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chk_ciudad_cba = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_calle = new System.Windows.Forms.TextBox();
            this.txt_numeracion = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtp_fecha_nacimiento = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_n_documento = new System.Windows.Forms.TextBox();
            this.txt_telefono = new System.Windows.Forms.TextBox();
            this.txt_apellido = new System.Windows.Forms.TextBox();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.cmb_tipos_documento = new TP_Criadero1.Clases.NuestroComboBox();
            this.cmb_barrio = new TP_Criadero1.Clases.NuestroComboBox();
            this.gpb_empleado.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpb_empleado
            // 
            this.gpb_empleado.Controls.Add(this.cmb_tipos_documento);
            this.gpb_empleado.Controls.Add(this.btn_registrar);
            this.gpb_empleado.Controls.Add(this.label9);
            this.gpb_empleado.Controls.Add(this.groupBox2);
            this.gpb_empleado.Controls.Add(this.groupBox3);
            this.gpb_empleado.Controls.Add(this.label5);
            this.gpb_empleado.Controls.Add(this.label3);
            this.gpb_empleado.Controls.Add(this.label2);
            this.gpb_empleado.Controls.Add(this.label1);
            this.gpb_empleado.Controls.Add(this.txt_n_documento);
            this.gpb_empleado.Controls.Add(this.txt_telefono);
            this.gpb_empleado.Controls.Add(this.txt_apellido);
            this.gpb_empleado.Controls.Add(this.txt_nombre);
            this.gpb_empleado.Location = new System.Drawing.Point(12, 12);
            this.gpb_empleado.Name = "gpb_empleado";
            this.gpb_empleado.Size = new System.Drawing.Size(565, 274);
            this.gpb_empleado.TabIndex = 0;
            this.gpb_empleado.TabStop = false;
            this.gpb_empleado.Text = "Empleado";
            // 
            // btn_registrar
            // 
            this.btn_registrar.Location = new System.Drawing.Point(484, 232);
            this.btn_registrar.Name = "btn_registrar";
            this.btn_registrar.Size = new System.Drawing.Size(75, 23);
            this.btn_registrar.TabIndex = 7;
            this.btn_registrar.Text = "Registrar";
            this.btn_registrar.UseVisualStyleBackColor = true;
            this.btn_registrar.Click += new System.EventHandler(this.Btn_registrar_Click_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 159);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Tipo de documento*";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmb_barrio);
            this.groupBox2.Controls.Add(this.chk_ciudad_cba);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txt_calle);
            this.groupBox2.Controls.Add(this.txt_numeracion);
            this.groupBox2.Location = new System.Drawing.Point(313, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 133);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Direccion";
            // 
            // chk_ciudad_cba
            // 
            this.chk_ciudad_cba.AutoSize = true;
            this.chk_ciudad_cba.Location = new System.Drawing.Point(43, 99);
            this.chk_ciudad_cba.Name = "chk_ciudad_cba";
            this.chk_ciudad_cba.Size = new System.Drawing.Size(96, 17);
            this.chk_ciudad_cba.TabIndex = 6;
            this.chk_ciudad_cba.Text = "Ciudad de Cba";
            this.chk_ciudad_cba.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Barrio";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Calle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Numeracion";
            // 
            // txt_calle
            // 
            this.txt_calle.Location = new System.Drawing.Point(75, 46);
            this.txt_calle.Name = "txt_calle";
            this.txt_calle.Size = new System.Drawing.Size(119, 20);
            this.txt_calle.TabIndex = 1;
            // 
            // txt_numeracion
            // 
            this.txt_numeracion.Location = new System.Drawing.Point(75, 23);
            this.txt_numeracion.Name = "txt_numeracion";
            this.txt_numeracion.Size = new System.Drawing.Size(119, 20);
            this.txt_numeracion.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtp_fecha_nacimiento);
            this.groupBox3.Location = new System.Drawing.Point(69, 201);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(225, 54);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "fecha de nacimiento";
            // 
            // dtp_fecha_nacimiento
            // 
            this.dtp_fecha_nacimiento.Location = new System.Drawing.Point(9, 19);
            this.dtp_fecha_nacimiento.Name = "dtp_fecha_nacimiento";
            this.dtp_fecha_nacimiento.Size = new System.Drawing.Size(200, 20);
            this.dtp_fecha_nacimiento.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 123);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "*n° documento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 92);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "telefono";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 62);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "*Apellido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 28);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "*Nombre";
            // 
            // txt_n_documento
            // 
            this.txt_n_documento.Location = new System.Drawing.Point(119, 126);
            this.txt_n_documento.Name = "txt_n_documento";
            this.txt_n_documento.Size = new System.Drawing.Size(146, 20);
            this.txt_n_documento.TabIndex = 3;
            // 
            // txt_telefono
            // 
            this.txt_telefono.Location = new System.Drawing.Point(119, 95);
            this.txt_telefono.Name = "txt_telefono";
            this.txt_telefono.Size = new System.Drawing.Size(146, 20);
            this.txt_telefono.TabIndex = 2;
            // 
            // txt_apellido
            // 
            this.txt_apellido.Location = new System.Drawing.Point(119, 62);
            this.txt_apellido.Name = "txt_apellido";
            this.txt_apellido.Size = new System.Drawing.Size(146, 20);
            this.txt_apellido.TabIndex = 1;
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(119, 28);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(146, 20);
            this.txt_nombre.TabIndex = 0;
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(551, 293);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 1;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click_1);
            // 
            // cmb_tipos_documento
            // 
            this.cmb_tipos_documento.descriptor = "descripcion";
            this.cmb_tipos_documento.FormattingEnabled = true;
            this.cmb_tipos_documento.Location = new System.Drawing.Point(119, 156);
            this.cmb_tipos_documento.Name = "cmb_tipos_documento";
            this.cmb_tipos_documento.nombre_tabla = "tiposDNI";
            this.cmb_tipos_documento.pk = "idtiposdni";
            this.cmb_tipos_documento.Size = new System.Drawing.Size(146, 21);
            this.cmb_tipos_documento.TabIndex = 34;
            this.cmb_tipos_documento.validar = false;
            this.cmb_tipos_documento.SelectedIndexChanged += new System.EventHandler(this.Cmb_tipos_documento_SelectedIndexChanged);
            // 
            // cmb_barrio
            // 
            this.cmb_barrio.descriptor = "nombre";
            this.cmb_barrio.FormattingEnabled = true;
            this.cmb_barrio.Location = new System.Drawing.Point(75, 72);
            this.cmb_barrio.Name = "cmb_barrio";
            this.cmb_barrio.nombre_tabla = "barrios";
            this.cmb_barrio.pk = "idbarrio";
            this.cmb_barrio.Size = new System.Drawing.Size(121, 21);
            this.cmb_barrio.TabIndex = 7;
            this.cmb_barrio.validar = false;
            // 
            // Frm_registrar_empleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(638, 328);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.gpb_empleado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_registrar_empleado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_registrar_empleado";
            this.Load += new System.EventHandler(this.Frm_registrar_empleado_Load);
            this.gpb_empleado.ResumeLayout(false);
            this.gpb_empleado.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpb_empleado;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chk_ciudad_cba;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_calle;
        private System.Windows.Forms.TextBox txt_numeracion;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_n_documento;
        private System.Windows.Forms.TextBox txt_telefono;
        private System.Windows.Forms.TextBox txt_apellido;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.DateTimePicker dtp_fecha_nacimiento;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_registrar;
        private Clases.NuestroComboBox cmb_tipos_documento;
        private Clases.NuestroComboBox cmb_barrio;
    }
}