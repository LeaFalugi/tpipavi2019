﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Empleado : Form
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_empleados = new DataTable();

        public Frm_ABM_Empleado()
        {
            InitializeComponent();
        }

        //METODOS PRIVADOS
        private void cargar_grilla(DataTable table)
        {
            dgv_empleados.Rows.Clear();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                dgv_empleados.Rows.Add();
                dgv_empleados.Rows[i].Cells[0].Value = table.Rows[i]["dni"].ToString();
                dgv_empleados.Rows[i].Cells[1].Value = table.Rows[i]["tipoDNI"].ToString();

                dgv_empleados.Rows[i].Cells[2].Value = table.Rows[i]["Nombre"].ToString() + " ";
                dgv_empleados.Rows[i].Cells[2].Value += table.Rows[i]["Apellido"].ToString();

                dgv_empleados.Rows[i].Cells[3].Value = table.Rows[i]["calle"].ToString() + " ";
                dgv_empleados.Rows[i].Cells[3].Value += table.Rows[i]["nro"].ToString();

                dgv_empleados.Rows[i].Cells[4].Value = table.Rows[i]["fechanacimiento"].ToString();
                dgv_empleados.Rows[i].Cells[5].Value = table.Rows[i]["telefono"].ToString();
            }

        }

        private void actualizar_grilla()
        {
            tabla_empleados = bd.ejecutar_consulta("exec grilla_empleados");
            cargar_grilla(tabla_empleados);
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Empleado buscar_Empleado = new Frm_Buscar_Empleado();
            buscar_Empleado.ShowDialog();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Empleado modificar_empleado = new Frm_Modificar_Empleado();

            modificar_empleado.tipoDni = dgv_empleados.CurrentRow.Cells[1].Value.ToString();
            modificar_empleado.dni = dgv_empleados.CurrentRow.Cells[0].Value.ToString();

            modificar_empleado.ShowDialog();
            actualizar_grilla();

        }

        private void Frm_ABM_Empleado_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_Registrar_Click(object sender, EventArgs e)
        {
            Frm_registrar_empleado registrar_Empleado = new Frm_registrar_empleado();
            registrar_Empleado.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_mostrar_todos_Click(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_empleados.CurrentRow == null)
            {
                return;
            }

            string dniEmplSelec = dgv_empleados.CurrentRow.Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea eliminar el empleado con DNI " +
                dniEmplSelec + "? ", "Baja Empleado", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {


                Clases.Negocios.NG_empleado NG_Empleado = new Clases.Negocios.NG_empleado();
                NG_Empleado.baja_Empleado(dniEmplSelec);


                MessageBox.Show("Empleado dado de baja", "Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabla_empleados = bd.ejecutar_consulta("exec grilla_empleados");
                cargar_grilla(tabla_empleados);

            }
            actualizar_grilla();
        }
    }
}
