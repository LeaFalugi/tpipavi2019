﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Criadero1
{
    public partial class Frm_Buscar_Empleado : Form
    {
        public int tipo_documento
        {
            get { return cmb_tipo_dni.SelectedIndex; }
            set { cmb_tipo_dni.SelectedIndex = value; }
        }

        public string dni
        {
            get { return txt_dni.Text; }
            set { txt_dni.Text = value; }
        }
        public Frm_Buscar_Empleado()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Buscar_Empleado_Load(object sender, EventArgs e)
        {
            
        }

        private void Btn_buscar_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
