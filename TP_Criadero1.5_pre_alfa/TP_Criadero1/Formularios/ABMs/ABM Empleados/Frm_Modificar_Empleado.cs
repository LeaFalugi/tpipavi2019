﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Modificar_Empleado : Form
    {
        NG_empleado ng_emp = new NG_empleado();
        BE_Direcciones be_direccion = new BE_Direcciones();

        public string tipoDni { get; set; }
        public string dni { get; set; }

        public string nombre
        {
            get { return txt_nombre.Text; }
            set { txt_nombre.Text = value; }
        }

        public string apellido
        {
            get { return txt_apellido.Text; }
            set { txt_apellido.Text = value; }
        }

        public int n_documento
        {
            get { return int.Parse(txt_n_documento.Text); }
            set { txt_n_documento.Text = value.ToString(); }
        }

        public Frm_Modificar_Empleado()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Modificar_Empleado_Load(object sender, EventArgs e)
        {
            cmb_barrio.cargar();
            cmb_tipo_documento.cargar();
            recuperar_datos1(tipoDni, dni);

        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();

            if (txt_nombre.Text == ""
               || txt_apellido.Text == "")
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nombre.Focus();
                return;
            }
            else
            {
                string idDir = be_direccion.registrar_direccion(txt_calle.Text,
                    int.Parse(cmb_barrio.SelectedValue.ToString()), txt_numeracion.Text);

                string fecha = "";
                if (dtp_fecha_nac.Value.Date != DateTime.Now.Date)
                {
                    fecha = date.convert_fecha(dtp_fecha_nac.Value);
                }

                ng_emp.modificar_empleado(n_documento.ToString(), cmb_tipo_documento.SelectedValue.ToString()
                    , nombre, apellido, fecha, txt_tel.Text, idDir);

                MessageBox.Show("Se modificaron los datos efectivamente.");
                this.Close();
            }
        }

        private void recuperar_datos1(string tipoDni, string dni)
        {
            DataTable tabla = new DataTable();
            tabla = ng_emp.recuperar_datos(tipoDni, dni);
            this.txt_nombre.Text = tabla.Rows[0]["Nombre"].ToString();
            this.txt_apellido.Text = tabla.Rows[0]["Apellido"].ToString();
            this.txt_tel.Text = tabla.Rows[0]["Telefono"].ToString();
            this.txt_n_documento.Text = dni;
            this.cmb_tipo_documento.SelectedValue = int.Parse(tabla.Rows[0][1].ToString());
            this.txt_calle.Text = tabla.Rows[0]["calle"].ToString();
            this.txt_numeracion.Text = tabla.Rows[0]["nro"].ToString();
            this.cmb_barrio.SelectedValue = int.Parse(tabla.Rows[0]["IdBarrio"].ToString()) + 1;
            if (tabla.Rows[0][7].ToString() != "")
            {
                this.dtp_fecha_nac.Value = DateTime.Parse(tabla.Rows[0][7].ToString());
            }
        }
    }
}
