﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;


namespace TP_Criadero1
{
    public partial class Frm_registrar_arcticulo : Form
    {
        public Frm_registrar_arcticulo()
        {
            InitializeComponent();
        }

        private void Frm_registrar_arcticulo_Load(object sender, EventArgs e)
        {
            DataTable proveedores = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            proveedores = bd.ejecutar_consulta("select Nombre, CUIT from Provedores");

            cmb_proveedor.DataSource = proveedores;
            cmb_proveedor.DisplayMember = "nombre";
            cmb_proveedor.ValueMember = "cuit";
        }

        private void Btn_nuevo_proveedor_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Proveedor registrar_Proveedor = new Frm_Registrar_Proveedor();
            registrar_Proveedor.ShowDialog();
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            NG_articulos articulo = new NG_articulos();
            
            if (txt_nombre.Text==""
                || txt_precio.Text==""
                || cmb_proveedor.SelectedIndex == -1)
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nombre.Focus();
                return;
            }
            else
            {
                articulo.registrar_articulo(txt_nombre.Text, txt_precio.Text
                    , cmb_proveedor.SelectedValue.ToString(), rtxt_descripcion.Text);

                MessageBox.Show("Se registro el articulo con exito.");
                this.Close();
            }


        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
