﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_articulos : Form
    {
        DataTable tabla_articulos = new DataTable();
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

        private void cargar_grilla(DataTable tabla)
        {
            dgv_articulos.Rows.Clear();

            for (int i=0; i < tabla.Rows.Count; i++)
            {
                
                dgv_articulos.Rows.Add();
                dgv_articulos.Rows[i].Cells[0].Value = tabla.Rows[i]["idarticulos"].ToString();
                dgv_articulos.Rows[i].Cells[1].Value = tabla.Rows[i]["nombre"].ToString();
                dgv_articulos.Rows[i].Cells[2].Value = tabla.Rows[i]["preciounitario"].ToString();
                dgv_articulos.Rows[i].Cells[3].Value = tabla.Rows[i]["Proveedor"].ToString();
                dgv_articulos.Rows[i].Cells[4].Value = tabla.Rows[i]["descripcion"].ToString();
                dgv_articulos.Rows[i].Cells[5].Value = tabla.Rows[i]["Disponibilidad"].ToString();
                
            }
        }
        private void actualizar_grilla()
        {
            tabla_articulos = bd.ejecutar_consulta("exec grilla_articulos");
            cargar_grilla(tabla_articulos);
        }
        public Frm_ABM_articulos()
        {
            InitializeComponent();
        }

        private void Frm_ABM_articulos_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_buscar_articulo buscar_Articulo = new Frm_buscar_articulo();
            buscar_Articulo.ShowDialog();

            if (buscar_Articulo.cancelada==false)
            {
                cargar_grilla(buscar_Articulo.tabla_busqueda);
            }
            buscar_Articulo.Dispose();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_registrar_arcticulo registrar_Arcticulo = new Frm_registrar_arcticulo();
            registrar_Arcticulo.ShowDialog();
            actualizar_grilla();
            
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_modificar_articulos modificar_Articulos = new Frm_modificar_articulos();

            modificar_Articulos.id_art = dgv_articulos.CurrentRow.Cells[0].Value.ToString();
            
            modificar_Articulos.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_articulos.CurrentRow == null)
            {
                return;
            }

            string articuloSelec = dgv_articulos.CurrentRow.Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea eliminar el articulo: " +
                articuloSelec + "? ", "Baja Articulo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {


                Clases.Negocios.NG_articulos NG_Articulos = new Clases.Negocios.NG_articulos();
                NG_Articulos.baja_Articulo(articuloSelec);


                MessageBox.Show("Articulo Eliminado", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabla_articulos = bd.ejecutar_consulta("exec grilla_articulos");
                cargar_grilla(tabla_articulos);

            }
            actualizar_grilla();
        }
    }
}
