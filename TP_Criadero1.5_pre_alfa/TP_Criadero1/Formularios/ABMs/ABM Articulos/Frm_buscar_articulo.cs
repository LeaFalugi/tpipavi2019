﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_buscar_articulo : Form
    {
        public DataTable tabla_busqueda { get; set; }
        public bool cancelada { get; set; }
        public Frm_buscar_articulo()
        {
            InitializeComponent();
        }

        private void Btn_buscar_Click(object sender, EventArgs e)
        {
            NG_articulos ng_art = new NG_articulos();
            string sql = "";

            if (chk_nombre.Checked)
            {
                sql += "where a.nombre like '%" + txt_n_articulo.Text + "%'";
                tabla_busqueda= ng_art.buscar_articulo(sql);
                cancelada = false;
                this.Close();
            }
            if (chk_precio.Checked)
            {
                sql += "where a.precioUnitario < " + txt_precio.Text;
                tabla_busqueda = ng_art.buscar_articulo(sql);
                cancelada = false;
                this.Close();

            }
            if (chk_proveedor.Checked)
            {
                sql += "where a.idProvedor = '" + cmb_proveedor.SelectedValue.ToString() + "'";
                tabla_busqueda = ng_art.buscar_articulo(sql);
                cancelada = false;
                this.Close();

            }
            else
            {
                MessageBox.Show("No seleccionaste nada.");
                txt_n_articulo.Focus();
                return;
            }
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                               "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void chk_nombre_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_nombre.Checked)
            {
                txt_n_articulo.Enabled = true;
                cmb_proveedor.Enabled = false;
                txt_precio.Enabled = false;
                chk_precio.Checked = false;
                chk_proveedor.Checked = false;
            }
            else
            {
                txt_n_articulo.Enabled = false;
            }
        }

        private void chk_proveedor_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_proveedor.Checked)
            {
                cmb_proveedor.Enabled = true;

                txt_n_articulo.Enabled = false;
                txt_precio.Enabled = false;
                chk_nombre.Checked = false;
                chk_precio.Checked = false;
            }
            else
            {
                cmb_proveedor.Enabled = false;
            }
        }

        private void chk_precio_CheckedChanged(object sender, EventArgs e)
        {
            if(chk_precio.Checked)
            {
                txt_precio.Enabled = true;

                txt_n_articulo.Enabled = false;
                cmb_proveedor.Enabled = false;
                chk_nombre.Checked = false;
                chk_proveedor.Checked = false;
            }
            else
            {
                txt_precio.Enabled = false;
            }
        }

        private void Frm_buscar_articulo_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();
            cancelada = true;

            string sql = "select CUIT, Nombre from Provedores";

            tabla = bd.ejecutar_consulta(sql);

            cmb_proveedor.DataSource = tabla;
            cmb_proveedor.DisplayMember = "nombre";
            cmb_proveedor.ValueMember = "cuit";
        }
    }
}
