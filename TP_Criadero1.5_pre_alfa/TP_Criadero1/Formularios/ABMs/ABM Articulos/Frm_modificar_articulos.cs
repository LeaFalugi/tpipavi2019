﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_modificar_articulos : Form
    {

        public string nombre
        {
            get { return txt_nombre.Text; }
            set { txt_nombre.Text = value; }
        }

        public string precio
        {
            get { return txt_precio.Text; }
            set { txt_precio.Text = value; }
        }

        public string prov
        {
            get { return cmb_proveedor.SelectedText; }
            set { cmb_proveedor.SelectedText = value; }
        }

        public string Desc 
            {
                get { return rtxt_descripcion.Text; }
                set { rtxt_descripcion.Text = value; }
             }


        public string id_art { get; set; }

        public string disponibilidad { get; set; }

        private void recuperar_datos()
        {
            NG_articulos ng_art = new NG_articulos();
            DataTable tabla_art = new DataTable();

            tabla_art = ng_art.recuperar_datos(id_art);

            txt_nombre.Text = tabla_art.Rows[0]["nombre"].ToString();
            txt_precio.Text = tabla_art.Rows[0]["preciounitario"].ToString();
            rtxt_descripcion.Text = tabla_art.Rows[0]["descripcion"].ToString();
            cmb_proveedor.SelectedValue = tabla_art.Rows[0]["proveedor"].ToString();
            disponibilidad = tabla_art.Rows[0]["disponibilidad"].ToString();

        }
        public Frm_modificar_articulos()
        {
            InitializeComponent();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD BE_BD = new BE_conexion_criadero_BD();
            NG_articulos articulo = new NG_articulos();

            articulo.modificar_articulo(id_art, txt_nombre.Text,
                rtxt_descripcion.Text, txt_precio.Text,
                cmb_proveedor.SelectedValue.ToString());

            MessageBox.Show("Se modificaron los datos del articulo con exito.");
            this.Close();
            
        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_modificar_articulos_Load(object sender, EventArgs e)
        {
            DataTable proveedores = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            proveedores = bd.ejecutar_consulta("select Nombre, CUIT from Provedores");

            cmb_proveedor.DataSource = proveedores;
            cmb_proveedor.DisplayMember = "nombre";
            cmb_proveedor.ValueMember = "cuit";

            recuperar_datos();

            lbl_disponibilidad.Text = "Disponibilidad: " + disponibilidad + " en stock";

        }

        private void Btn_nuevo_proveedor_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Proveedor reg_proveedor = new Frm_Registrar_Proveedor();

            reg_proveedor.ShowDialog();
        }
    }
}
