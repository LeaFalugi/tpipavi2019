﻿namespace TP_Criadero1
{
    partial class Frm_buscar_articulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_buscar_articulo));
            this.gpb_articulos = new System.Windows.Forms.GroupBox();
            this.chk_nombre = new System.Windows.Forms.CheckBox();
            this.chk_precio = new System.Windows.Forms.CheckBox();
            this.chk_proveedor = new System.Windows.Forms.CheckBox();
            this.txt_precio = new System.Windows.Forms.TextBox();
            this.cmb_proveedor = new System.Windows.Forms.ComboBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.txt_n_articulo = new System.Windows.Forms.TextBox();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.gpb_articulos.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpb_articulos
            // 
            this.gpb_articulos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpb_articulos.Controls.Add(this.chk_nombre);
            this.gpb_articulos.Controls.Add(this.chk_precio);
            this.gpb_articulos.Controls.Add(this.chk_proveedor);
            this.gpb_articulos.Controls.Add(this.txt_precio);
            this.gpb_articulos.Controls.Add(this.cmb_proveedor);
            this.gpb_articulos.Controls.Add(this.btn_buscar);
            this.gpb_articulos.Controls.Add(this.txt_n_articulo);
            this.gpb_articulos.Location = new System.Drawing.Point(12, 12);
            this.gpb_articulos.Name = "gpb_articulos";
            this.gpb_articulos.Size = new System.Drawing.Size(394, 253);
            this.gpb_articulos.TabIndex = 0;
            this.gpb_articulos.TabStop = false;
            this.gpb_articulos.Text = "Articulos";
            // 
            // chk_nombre
            // 
            this.chk_nombre.AutoSize = true;
            this.chk_nombre.Location = new System.Drawing.Point(261, 61);
            this.chk_nombre.Name = "chk_nombre";
            this.chk_nombre.Size = new System.Drawing.Size(63, 17);
            this.chk_nombre.TabIndex = 9;
            this.chk_nombre.Text = "Nombre";
            this.chk_nombre.UseVisualStyleBackColor = true;
            this.chk_nombre.CheckedChanged += new System.EventHandler(this.chk_nombre_CheckedChanged);
            // 
            // chk_precio
            // 
            this.chk_precio.AutoSize = true;
            this.chk_precio.Location = new System.Drawing.Point(261, 153);
            this.chk_precio.Name = "chk_precio";
            this.chk_precio.Size = new System.Drawing.Size(123, 17);
            this.chk_precio.TabIndex = 8;
            this.chk_precio.Text = "Precio: menos que...";
            this.chk_precio.UseVisualStyleBackColor = true;
            this.chk_precio.CheckedChanged += new System.EventHandler(this.chk_precio_CheckedChanged);
            // 
            // chk_proveedor
            // 
            this.chk_proveedor.AutoSize = true;
            this.chk_proveedor.Location = new System.Drawing.Point(261, 108);
            this.chk_proveedor.Name = "chk_proveedor";
            this.chk_proveedor.Size = new System.Drawing.Size(75, 17);
            this.chk_proveedor.TabIndex = 7;
            this.chk_proveedor.Text = "Proveedor";
            this.chk_proveedor.UseVisualStyleBackColor = true;
            this.chk_proveedor.CheckedChanged += new System.EventHandler(this.chk_proveedor_CheckedChanged);
            // 
            // txt_precio
            // 
            this.txt_precio.Enabled = false;
            this.txt_precio.Location = new System.Drawing.Point(64, 151);
            this.txt_precio.Name = "txt_precio";
            this.txt_precio.Size = new System.Drawing.Size(172, 20);
            this.txt_precio.TabIndex = 5;
            // 
            // cmb_proveedor
            // 
            this.cmb_proveedor.Enabled = false;
            this.cmb_proveedor.FormattingEnabled = true;
            this.cmb_proveedor.Location = new System.Drawing.Point(64, 106);
            this.cmb_proveedor.Name = "cmb_proveedor";
            this.cmb_proveedor.Size = new System.Drawing.Size(172, 21);
            this.cmb_proveedor.TabIndex = 4;
            // 
            // btn_buscar
            // 
            this.btn_buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_buscar.Location = new System.Drawing.Point(306, 224);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 1;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.Btn_buscar_Click);
            // 
            // txt_n_articulo
            // 
            this.txt_n_articulo.Enabled = false;
            this.txt_n_articulo.Location = new System.Drawing.Point(64, 59);
            this.txt_n_articulo.Name = "txt_n_articulo";
            this.txt_n_articulo.Size = new System.Drawing.Size(172, 20);
            this.txt_n_articulo.TabIndex = 0;
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cancelar.Location = new System.Drawing.Point(366, 278);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 1;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // Frm_buscar_articulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(452, 312);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.gpb_articulos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_buscar_articulo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscar Articulo";
            this.Load += new System.EventHandler(this.Frm_buscar_articulo_Load);
            this.gpb_articulos.ResumeLayout(false);
            this.gpb_articulos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpb_articulos;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox txt_n_articulo;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.ComboBox cmb_proveedor;
        private System.Windows.Forms.CheckBox chk_precio;
        private System.Windows.Forms.CheckBox chk_proveedor;
        private System.Windows.Forms.TextBox txt_precio;
        private System.Windows.Forms.CheckBox chk_nombre;
    }
}