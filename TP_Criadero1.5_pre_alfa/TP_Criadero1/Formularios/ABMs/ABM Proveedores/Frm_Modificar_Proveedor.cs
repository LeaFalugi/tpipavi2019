﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Modificar_Proveedor : Form
    {
        public string idDireccion { get; set; }
        public string cuit { get; set; }
        private void recuperar_datos()
        {
            NG_proveedores ng_prov = new NG_proveedores();
            BE_Direcciones dire = new BE_Direcciones();
            DataTable tabla_p = new DataTable();
            DataTable tabla_d = new DataTable();

            tabla_p = ng_prov.recuperar_datos(cuit);

            txt_cuit.Text = cuit;
            txt_nombre.Text = tabla_p.Rows[0]["nombre"].ToString();
            txt_Telefono.Text= tabla_p.Rows[0]["telefono"].ToString();
            txt_email.Text= tabla_p.Rows[0]["mail"].ToString();
            idDireccion = tabla_p.Rows[0]["idDireccion"].ToString();

            tabla_d = dire.recuperar_datos(idDireccion);

            txt_calle.Text = tabla_d.Rows[0]["calle"].ToString();
            txt_numeracion.Text= tabla_d.Rows[0]["nro"].ToString();
            cmb_barrio.SelectedValue= tabla_d.Rows[0]["idBarrio"].ToString();

        }
        public Frm_Modificar_Proveedor()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Modificar_Proveedor_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select * from barrios";

            tabla = bd.ejecutar_consulta(sql);

            cmb_barrio.DataSource = tabla;
            cmb_barrio.DisplayMember = "nombre";
            cmb_barrio.ValueMember = "idBarrio";

            recuperar_datos();

        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            NG_proveedores ng_prov = new NG_proveedores();
            BE_Direcciones dire = new BE_Direcciones();

            if (txt_nombre.Text==""
                ||txt_calle.Text==""
                || txt_numeracion.Text == "")
            {
                MessageBox.Show("Faltan datos obligatorios por cargar (Los que tienen un asterisco *).");
                txt_nombre.Focus();
                return;
            }
            else
            {
                dire.modificar_direccion(idDireccion, txt_calle.Text,
                    txt_numeracion.Text, cmb_barrio.SelectedValue.ToString());

                ng_prov.modificar_proveedor(cuit, txt_nombre.Text, txt_Telefono.Text,
                    txt_email.Text, idDireccion);

                MessageBox.Show("Se modificaron los datos del proveedor con exito.");
                this.Close();
            }
        }
    }
}
