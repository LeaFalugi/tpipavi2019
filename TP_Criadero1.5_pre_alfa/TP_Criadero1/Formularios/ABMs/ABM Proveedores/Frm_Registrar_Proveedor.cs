﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Registrar_Proveedor : Form
    {
        NG_proveedores ng_prov = new NG_proveedores();
        BE_Direcciones be_dir = new BE_Direcciones();
        public Frm_Registrar_Proveedor()
        {
            InitializeComponent();
        }

        private void Frm_Registrar_Proveedor_Load(object sender, EventArgs e)
        {
            cmb_barrio.cargar();

        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            

            if (txt_nombre.Text==""
                || txt_cuit.Text ==""
                || txt_calle.Text ==""
                || txt_numeracion.Text==""
                || cmb_barrio.SelectedIndex == -1)
            {
                MessageBox.Show("Faltan datos obligatorios por cargar (Los que tienen un asterisco *).");
                txt_nombre.Focus();
            }
            else
            {
                string idDir = be_dir.registrar_direccion(txt_calle.Text,
                    int.Parse(cmb_barrio.SelectedValue.ToString()), txt_numeracion.Text);

                ng_prov.registrar_proveedor(txt_cuit.Text, txt_nombre.Text,
                    txt_Telefono.Text, idDir, txt_email.Text);

                MessageBox.Show("Se registro el proveedor con exito.");
                this.Close();

            }
        }
    }
}
