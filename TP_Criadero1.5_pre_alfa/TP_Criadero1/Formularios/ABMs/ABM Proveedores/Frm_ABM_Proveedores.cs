﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Proveedores : Form
    {
        DataTable tabla_proveedores = new DataTable();
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

        public Frm_ABM_Proveedores()
        {
            InitializeComponent();
        }

        private void cargar_grilla(DataTable tabla)
        {
            dgv_proveedores.Rows.Clear();

            for (int i=0; i < tabla.Rows.Count; i++)
            {
                dgv_proveedores.Rows.Add();
                dgv_proveedores.Rows[i].Cells[0].Value = tabla.Rows[i]["nombre"].ToString();
                dgv_proveedores.Rows[i].Cells[1].Value = tabla.Rows[i]["cuit"].ToString();
                dgv_proveedores.Rows[i].Cells[2].Value = tabla.Rows[i]["telefono"].ToString();
                dgv_proveedores.Rows[i].Cells[3].Value = tabla.Rows[i]["mail"].ToString();

                dgv_proveedores.Rows[i].Cells[4].Value = tabla.Rows[i]["calle"].ToString() + " ";
                dgv_proveedores.Rows[i].Cells[4].Value += tabla.Rows[i]["nro"].ToString();
            }
        }

        private void actualizar_grilla()
        {
            tabla_proveedores = bd.ejecutar_consulta("exec grilla_proveedores");
            cargar_grilla(tabla_proveedores);
        }
        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Frm_ABM_Proveedores_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Proveedor buscar_proveedor = new Frm_Buscar_Proveedor();
            buscar_proveedor.ShowDialog();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Proveedor registrar_Proveedor = new Frm_Registrar_Proveedor();
            registrar_Proveedor.ShowDialog();
            actualizar_grilla();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Proveedor modificar_Proveedor = new Frm_Modificar_Proveedor();
            modificar_Proveedor.cuit = dgv_proveedores.CurrentRow.Cells[1].Value.ToString();
            modificar_Proveedor.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_proveedores.CurrentRow == null)
            {
                return;
            }

            string cuitSeleccionado = dgv_proveedores.CurrentRow.Cells[1].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea eliminar el proveedor con CUIT: " +
                cuitSeleccionado + "? ", "Baja Proveedor", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                Clases.Negocios.NG_proveedores NG_Proveedores = new Clases.Negocios.NG_proveedores();

                NG_Proveedores.baja_Proveedor(dgv_proveedores.CurrentRow.Cells[1].Value.ToString());

                MessageBox.Show("Proveedor Eliminado", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                tabla_proveedores = bd.ejecutar_consulta("exec grilla_proveedores");
                cargar_grilla(tabla_proveedores);
            }

            actualizar_grilla();
        }
    }
}
