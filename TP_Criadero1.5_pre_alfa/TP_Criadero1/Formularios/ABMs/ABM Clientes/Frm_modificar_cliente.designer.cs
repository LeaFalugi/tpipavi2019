﻿namespace TP_Criadero1
{
    partial class Frm_modificar_cliente
    {   
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_modificar_cliente));
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmb_barrio = new TP_Criadero1.Clases.NuestroComboBox();
            this.chk_ciudad_cba = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_calle = new System.Windows.Forms.TextBox();
            this.txt_numeracion = new System.Windows.Forms.TextBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_mail = new System.Windows.Forms.TextBox();
            this.txt_telefono = new System.Windows.Forms.TextBox();
            this.txt_apellido = new System.Windows.Forms.TextBox();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.btn_restaurar = new System.Windows.Forms.Button();
            this.txt_n_documento = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grp_clientes = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmb_tipo_documento = new TP_Criadero1.Clases.NuestroComboBox();
            this.groupBox2.SuspendLayout();
            this.grp_clientes.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(354, 476);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 10;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.Location = new System.Drawing.Point(286, 429);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(75, 23);
            this.btn_guardar.TabIndex = 8;
            this.btn_guardar.Text = "Modificar";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.Btn_registrar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmb_barrio);
            this.groupBox2.Controls.Add(this.chk_ciudad_cba);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txt_calle);
            this.groupBox2.Controls.Add(this.txt_numeracion);
            this.groupBox2.Location = new System.Drawing.Point(37, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 133);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Direccion";
            // 
            // cmb_barrio
            // 
            this.cmb_barrio.descriptor = "nombre";
            this.cmb_barrio.FormattingEnabled = true;
            this.cmb_barrio.Location = new System.Drawing.Point(105, 71);
            this.cmb_barrio.Name = "cmb_barrio";
            this.cmb_barrio.nombre_tabla = "barrios";
            this.cmb_barrio.pk = "idbarrio";
            this.cmb_barrio.Size = new System.Drawing.Size(200, 21);
            this.cmb_barrio.TabIndex = 34;
            this.cmb_barrio.validar = false;
            // 
            // chk_ciudad_cba
            // 
            this.chk_ciudad_cba.AutoSize = true;
            this.chk_ciudad_cba.Location = new System.Drawing.Point(81, 98);
            this.chk_ciudad_cba.Name = "chk_ciudad_cba";
            this.chk_ciudad_cba.Size = new System.Drawing.Size(96, 17);
            this.chk_ciudad_cba.TabIndex = 6;
            this.chk_ciudad_cba.Text = "Ciudad de Cba";
            this.chk_ciudad_cba.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(61, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Barrio";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Calle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Numeracion";
            // 
            // txt_calle
            // 
            this.txt_calle.Location = new System.Drawing.Point(105, 47);
            this.txt_calle.Name = "txt_calle";
            this.txt_calle.Size = new System.Drawing.Size(200, 20);
            this.txt_calle.TabIndex = 1;
            // 
            // txt_numeracion
            // 
            this.txt_numeracion.Location = new System.Drawing.Point(105, 23);
            this.txt_numeracion.Name = "txt_numeracion";
            this.txt_numeracion.Size = new System.Drawing.Size(200, 20);
            this.txt_numeracion.TabIndex = 0;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "yyMMdd";
            this.dateTimePicker2.Location = new System.Drawing.Point(142, 318);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 392);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "E-mail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 356);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Teléfono";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 147);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Apellido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 113);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Nombre";
            // 
            // txt_mail
            // 
            this.txt_mail.Location = new System.Drawing.Point(142, 388);
            this.txt_mail.Name = "txt_mail";
            this.txt_mail.Size = new System.Drawing.Size(200, 20);
            this.txt_mail.TabIndex = 3;
            this.txt_mail.TextChanged += new System.EventHandler(this.txt_mail_TextChanged);
            // 
            // txt_telefono
            // 
            this.txt_telefono.Location = new System.Drawing.Point(142, 353);
            this.txt_telefono.Name = "txt_telefono";
            this.txt_telefono.Size = new System.Drawing.Size(200, 20);
            this.txt_telefono.TabIndex = 2;
            // 
            // txt_apellido
            // 
            this.txt_apellido.Location = new System.Drawing.Point(142, 142);
            this.txt_apellido.Name = "txt_apellido";
            this.txt_apellido.Size = new System.Drawing.Size(200, 20);
            this.txt_apellido.TabIndex = 1;
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(142, 107);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(200, 20);
            this.txt_nombre.TabIndex = 0;
            // 
            // btn_restaurar
            // 
            this.btn_restaurar.Location = new System.Drawing.Point(199, 429);
            this.btn_restaurar.Name = "btn_restaurar";
            this.btn_restaurar.Size = new System.Drawing.Size(75, 23);
            this.btn_restaurar.TabIndex = 9;
            this.btn_restaurar.Text = "Restaurar ";
            this.btn_restaurar.UseVisualStyleBackColor = true;
            this.btn_restaurar.Click += new System.EventHandler(this.Btn_restaurar_Click);
            // 
            // txt_n_documento
            // 
            this.txt_n_documento.Enabled = false;
            this.txt_n_documento.Location = new System.Drawing.Point(142, 70);
            this.txt_n_documento.Name = "txt_n_documento";
            this.txt_n_documento.Size = new System.Drawing.Size(200, 20);
            this.txt_n_documento.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 73);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "N° documento";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Tipo de documento";
            // 
            // grp_clientes
            // 
            this.grp_clientes.Controls.Add(this.label10);
            this.grp_clientes.Controls.Add(this.dateTimePicker2);
            this.grp_clientes.Controls.Add(this.cmb_tipo_documento);
            this.grp_clientes.Controls.Add(this.groupBox2);
            this.grp_clientes.Controls.Add(this.label9);
            this.grp_clientes.Controls.Add(this.btn_restaurar);
            this.grp_clientes.Controls.Add(this.btn_guardar);
            this.grp_clientes.Controls.Add(this.txt_nombre);
            this.grp_clientes.Controls.Add(this.txt_apellido);
            this.grp_clientes.Controls.Add(this.label5);
            this.grp_clientes.Controls.Add(this.txt_telefono);
            this.grp_clientes.Controls.Add(this.label4);
            this.grp_clientes.Controls.Add(this.txt_mail);
            this.grp_clientes.Controls.Add(this.label3);
            this.grp_clientes.Controls.Add(this.txt_n_documento);
            this.grp_clientes.Controls.Add(this.label2);
            this.grp_clientes.Controls.Add(this.label1);
            this.grp_clientes.Location = new System.Drawing.Point(12, 12);
            this.grp_clientes.Name = "grp_clientes";
            this.grp_clientes.Size = new System.Drawing.Size(386, 457);
            this.grp_clientes.TabIndex = 34;
            this.grp_clientes.TabStop = false;
            this.grp_clientes.Text = "Clientes";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 325);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Fecha de Nacimiento";
            // 
            // cmb_tipo_documento
            // 
            this.cmb_tipo_documento.descriptor = "descripcion";
            this.cmb_tipo_documento.Enabled = false;
            this.cmb_tipo_documento.FormattingEnabled = true;
            this.cmb_tipo_documento.Location = new System.Drawing.Point(142, 32);
            this.cmb_tipo_documento.Name = "cmb_tipo_documento";
            this.cmb_tipo_documento.nombre_tabla = "tiposdni";
            this.cmb_tipo_documento.pk = "idtipodni";
            this.cmb_tipo_documento.Size = new System.Drawing.Size(200, 21);
            this.cmb_tipo_documento.TabIndex = 19;
            this.cmb_tipo_documento.validar = false;
            // 
            // Frm_modificar_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(447, 511);
            this.Controls.Add(this.grp_clientes);
            this.Controls.Add(this.btn_cancelar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_modificar_cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Datos de un Cliente";
            this.Load += new System.EventHandler(this.Frm_modificar_cliente_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grp_clientes.ResumeLayout(false);
            this.grp_clientes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chk_ciudad_cba;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_restaurar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox grp_clientes;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private Clases.NuestroComboBox cmb_barrio;
        private Clases.NuestroComboBox cmb_tipo_documento;
        private System.Windows.Forms.TextBox txt_calle;
        private System.Windows.Forms.TextBox txt_numeracion;
        private System.Windows.Forms.TextBox txt_mail;
        private System.Windows.Forms.TextBox txt_telefono;
        private System.Windows.Forms.TextBox txt_apellido;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.TextBox txt_n_documento;
        private System.Windows.Forms.Label label10;
    }
}