﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_modificar_cliente : Form
    {
        NG_clientes ng_cliente = new NG_clientes();
        BE_Direcciones dir = new BE_Direcciones();

        public string tipoDni { get; set; }
        public string dni { get; set; } 

        public string nombre
        {
            get { return txt_nombre.Text; }
            set { txt_nombre.Text = value; }
        }

        public string apellido
        {
            get { return txt_apellido.Text; }
            set { txt_apellido.Text = value; }
        }

        public int n_documento
        {
            get { return int.Parse(txt_n_documento.Text); }
            set { txt_n_documento.Text = value.ToString(); }
        }

        public Frm_modificar_cliente()
        {
            InitializeComponent();
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();

            if (txt_nombre.Text==""
                ||txt_apellido.Text=="")
            {
                MessageBox.Show("No se puede modificar Tipo y Numero de documento.");
                txt_nombre.Focus();
                return;
            }
            else
            {
                string id_dir = dir.registrar_direccion(txt_calle.Text,
                    int.Parse(cmb_barrio.SelectedValue.ToString()), txt_numeracion.Text);

                string fecha = "null";
                if (dateTimePicker2.Value.Date != DateTime.Now.Date)
                {
                    fecha = date.convert_fecha(dateTimePicker2.Value);
                }

                ng_cliente.modificar_cliente(txt_nombre.Text, txt_apellido.Text
                    , txt_telefono.Text, fecha , n_documento.ToString()
                    , cmb_tipo_documento.SelectedValue.ToString(), id_dir,txt_mail.Text);

                MessageBox.Show("Se modifico el cliente con exito.");

                this.Close();
            }
        }
        private void Cmb_barrio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            recuperar_datos1(tipoDni,dni);
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_modificar_cliente_Load(object sender, EventArgs e)
        {
            cmb_barrio.cargar();
            cmb_tipo_documento.cargar();
            recuperar_datos1(tipoDni,dni);
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txt_mail_TextChanged(object sender, EventArgs e)
        {

        }
        private void recuperar_datos1(string tipoDni, string dni)
        {
            DataTable tabla = new DataTable();
            tabla = ng_cliente.recuperar_datos(tipoDni, dni);
            this.txt_nombre.Text = tabla.Rows[0]["Nombre"].ToString();
            this.txt_apellido.Text = tabla.Rows[0]["Apellido"].ToString();
            this.txt_mail.Text = tabla.Rows[0]["Email"].ToString();
            this.txt_telefono.Text = tabla.Rows[0]["Telefono"].ToString();
            this.txt_n_documento.Text = dni;
            this.cmb_tipo_documento.SelectedValue = int.Parse(tipoDni);
            this.txt_calle.Text = tabla.Rows[0]["calle"].ToString();
            this.txt_numeracion.Text = tabla.Rows[0]["nro"].ToString();
            this.cmb_barrio.SelectedValue = int.Parse(tabla.Rows[0]["IdBarrio"].ToString()) + 1;
            if (tabla.Rows[0]["fechaNac"].ToString() != "")
                {
                    this.dateTimePicker2.Value = DateTime.Parse(tabla.Rows[0]["fechaNac"].ToString());
                }

        }
    }
}
