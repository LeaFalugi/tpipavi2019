﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    
    public partial class Frm_ABM_Cliente : Form
    {
        
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_clientes = new DataTable();

        public Frm_ABM_Cliente()
        {
            InitializeComponent();
        }

        private void cargar_grilla(DataTable tabla)
        {
            dgv_Clientes.Rows.Clear();

            for (int i=0; i < tabla.Rows.Count; i++)
            {
                dgv_Clientes.Rows.Add();
                dgv_Clientes.Rows[i].Cells[0].Value = tabla.Rows[i]["dni"].ToString();
                dgv_Clientes.Rows[i].Cells[1].Value = tabla.Rows[i]["descripcion"].ToString();

                dgv_Clientes.Rows[i].Cells[2].Value = tabla.Rows[i]["nombre"].ToString() + " ";
                dgv_Clientes.Rows[i].Cells[2].Value += tabla.Rows[i]["apellido"].ToString();

                dgv_Clientes.Rows[i].Cells[3].Value = tabla.Rows[i]["calle"].ToString() + " ";
                dgv_Clientes.Rows[i].Cells[3].Value += tabla.Rows[i]["nro"].ToString();

                dgv_Clientes.Rows[i].Cells[4].Value = tabla.Rows[i]["fechaNac"].ToString();
                dgv_Clientes.Rows[i].Cells[5].Value = tabla.Rows[i]["telefono"].ToString();
            }
        }
        
        private void actualizar_grilla()
        {
            tabla_clientes = bd.ejecutar_consulta("exec grilla_clientes");
            cargar_grilla(tabla_clientes);
        }
        private void Btn_registrar_cliente_Click(object sender, EventArgs e)
        {
            Frm_registrar_cliente regist_cliente = new Frm_registrar_cliente();

            regist_cliente.ShowDialog();
            actualizar_grilla();
            regist_cliente.Dispose();
        }


        private void Frm_buscar_cliente_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            
            Frm_modificar_cliente modificar_cliente = new Frm_modificar_cliente();
            
            modificar_cliente.tipoDni = dgv_Clientes.CurrentRow.Cells[1].Value.ToString();
            modificar_cliente.dni = dgv_Clientes.CurrentRow.Cells[0].Value.ToString();
             
            modificar_cliente.ShowDialog();
            actualizar_grilla();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_registrar_cliente registrar_cliente = new Frm_registrar_cliente();
            registrar_cliente.ShowDialog();

            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Cliente buscar_Cliente = new Frm_Buscar_Cliente();
            buscar_Cliente.ShowDialog();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_Clientes.CurrentRow == null)
            {
                return;
            }

            string dniClienteSelec = dgv_Clientes.CurrentRow.Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea eliminar el cliente con DNI " +
                dniClienteSelec + "? ", "Baja Cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {


                NG_clientes NG_Clientes = new NG_clientes();
                NG_Clientes.baja_cliente(dniClienteSelec);


                MessageBox.Show("Cliente dado de baja", "Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabla_clientes = bd.ejecutar_consulta("exec grilla_clientes");
                cargar_grilla(tabla_clientes);

            }
            actualizar_grilla();
        }
    }
}
