﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_registrar_cliente : Form
    {
        NG_clientes ng_cliente = new NG_clientes();

        public Frm_registrar_cliente()
        {
            InitializeComponent();
        }

        private void Frm_abm_clientes_Load(object sender, EventArgs e)
        {
            cmb_barrio.cargar();
            cmb_tipo_documento.cargar();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            BE_Direcciones dir = new BE_Direcciones();
            BE_fecha date = new BE_fecha();

            if (txt_n_documento.Text == ""
                || cmb_tipo_documento.SelectedIndex == -1
                || txt_nombre.Text == ""
                || txt_apellido.Text == "")
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nombre.Focus();
            }
            else
            {
                string id_dir = dir.registrar_direccion(txt_calle.Text,
                    int.Parse(cmb_barrio.SelectedValue.ToString()), txt_numeracion.Text);

                string fecha = date.convert_fecha(dtp_fechaNacimiento.Value);

                ng_cliente.registrar_cliente(txt_nombre.Text, txt_apellido.Text                    
                    , txt_n_documento.Text, cmb_tipo_documento.SelectedValue.ToString(),
                    id_dir,fecha, txt_telefono.Text,txt_email.Text);

                MessageBox.Show("Se registro el cliente con exito.");

                this.Close();
            }
            
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
