﻿namespace TP_Criadero1
{
    partial class Frm_Buscar_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Buscar_Cliente));
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.Usuario = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.cmb_tipo_dni = new System.Windows.Forms.ComboBox();
            this.txt_dni = new System.Windows.Forms.TextBox();
            this.Usuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(255, 207);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 1;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // Usuario
            // 
            this.Usuario.Controls.Add(this.label2);
            this.Usuario.Controls.Add(this.label1);
            this.Usuario.Controls.Add(this.btn_buscar);
            this.Usuario.Controls.Add(this.cmb_tipo_dni);
            this.Usuario.Controls.Add(this.txt_dni);
            this.Usuario.Location = new System.Drawing.Point(12, 12);
            this.Usuario.Name = "Usuario";
            this.Usuario.Size = new System.Drawing.Size(299, 177);
            this.Usuario.TabIndex = 0;
            this.Usuario.TabStop = false;
            this.Usuario.Text = "Cliente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Documento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tipo Documento";
            // 
            // btn_buscar
            // 
            this.btn_buscar.Location = new System.Drawing.Point(210, 134);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.Btn_buscar_Click);
            // 
            // cmb_tipo_dni
            // 
            this.cmb_tipo_dni.FormattingEnabled = true;
            this.cmb_tipo_dni.Location = new System.Drawing.Point(98, 35);
            this.cmb_tipo_dni.Name = "cmb_tipo_dni";
            this.cmb_tipo_dni.Size = new System.Drawing.Size(148, 21);
            this.cmb_tipo_dni.TabIndex = 0;
            // 
            // txt_dni
            // 
            this.txt_dni.Location = new System.Drawing.Point(98, 78);
            this.txt_dni.Name = "txt_dni";
            this.txt_dni.Size = new System.Drawing.Size(148, 20);
            this.txt_dni.TabIndex = 1;
            // 
            // Frm_Buscar_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(340, 245);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.Usuario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_Buscar_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscar Cliente";
            this.Load += new System.EventHandler(this.Frm_Buscar_Cliente_Load);
            this.Usuario.ResumeLayout(false);
            this.Usuario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.GroupBox Usuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.ComboBox cmb_tipo_dni;
        private System.Windows.Forms.TextBox txt_dni;
    }
}