﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Pedidos : Form
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_pedidos = new DataTable();

        private void cargar_grilla(DataTable tabla)
        {
            dgv_pedidos.Rows.Clear();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                dgv_pedidos.Rows.Add();

                dgv_pedidos.Rows[i].Cells[0].Value = tabla.Rows[i]["nro"].ToString();
                dgv_pedidos.Rows[i].Cells[1].Value = tabla.Rows[i]["Articulo"].ToString();
                dgv_pedidos.Rows[i].Cells[2].Value = tabla.Rows[i]["cantidad"].ToString();
                dgv_pedidos.Rows[i].Cells[3].Value = tabla.Rows[i]["proveedor"].ToString();
                dgv_pedidos.Rows[i].Cells[4].Value = tabla.Rows[i]["fechaemision"].ToString();
                dgv_pedidos.Rows[i].Cells[5].Value = tabla.Rows[i]["fechaefectiva"].ToString();
                dgv_pedidos.Rows[i].Cells[6].Value = tabla.Rows[i]["id_Provedor"].ToString();

            }
        }

        private void actualizar_grilla()
        {
            tabla_pedidos = bd.ejecutar_consulta("exec grilla_pedidos");
            cargar_grilla(tabla_pedidos);
        }
        public Frm_ABM_Pedidos()
        {
            InitializeComponent();
        }

        private void Frm_ABM_Pedidos_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_buscar_Pedido buscar_pedido = new Frm_buscar_Pedido();
            buscar_pedido.ShowDialog();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Pedido modificar_Pedido = new Frm_Modificar_Pedido();
            modificar_Pedido.cuit = dgv_pedidos.CurrentRow.Cells[6].Value.ToString();
            modificar_Pedido.nro_ped= dgv_pedidos.CurrentRow.Cells[0].Value.ToString();

            modificar_Pedido.ShowDialog();
            actualizar_grilla();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Pedido registrar_Pedido = new Frm_Registrar_Pedido();
            registrar_Pedido.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_pedidos.CurrentRow == null)
            {
                return;
            }

            string nroPedSelec = dgv_pedidos.CurrentRow.Cells[0].Value.ToString();
            string cuit = dgv_pedidos.CurrentRow.Cells[6].Value.ToString();
            Console.WriteLine("nro: " + nroPedSelec + " cuit: " + cuit);
            DialogResult result = MessageBox.Show("¿Esta seguro que desea dar de baja el pedido N° " +
                nroPedSelec + "? ", "Baja Pedido", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                Clases.Negocios.NG_pedidos NG_Pedidos = new Clases.Negocios.NG_pedidos();

                NG_Pedidos.baja_Pedido(nroPedSelec,cuit);

                MessageBox.Show("Pedido dado de baja", "Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);

                tabla_pedidos = bd.ejecutar_consulta("exec grilla_pedidos");
                cargar_grilla(tabla_pedidos);
            }
            actualizar_grilla();
        }
    }
}
