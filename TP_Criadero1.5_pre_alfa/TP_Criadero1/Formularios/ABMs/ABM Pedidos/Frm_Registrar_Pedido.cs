﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Registrar_Pedido : Form
    {
        private void cargar_grilla(DataTable tabla)
        {
            dgv_articulos.Rows.Clear();

            for (int i=0; i<tabla.Rows.Count; i++)
            {
                dgv_articulos.Rows.Add();
                dgv_articulos.Rows[i].Cells[0].Value = tabla.Rows[i]["idArticulos"].ToString();
                dgv_articulos.Rows[i].Cells[1].Value = tabla.Rows[i]["Nombre"].ToString();
            }
        }

        
        public Frm_Registrar_Pedido()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            BE_detallePedido detallePedido = new BE_detallePedido();
            NG_pedidos pedidos = new NG_pedidos();
            BE_fecha date = new BE_fecha();

            if (dgv_select_articulos.Rows.Count == 0
                || cmb_proveedores.Enabled==true)
            {
                MessageBox.Show("No se selecciono ningun Articulo o Proveedor.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                string fechaEMI = date.convert_fecha(DateTime.Now);
                pedidos.registrar_pedido(cmb_proveedores.SelectedValue.ToString(),
                    fechaEMI,null);

                int nro_ped = pedidos.id_ultimo_pedido_registrado();

                for (int i=0; i<dgv_select_articulos.Rows.Count; i++)
                {
                    string id_art = dgv_select_articulos.Rows[i].Cells[0].Value.ToString();
                    string cant = dgv_select_articulos.Rows[i].Cells[0].Value.ToString();

                    detallePedido.registrar_detalle(nro_ped.ToString(),
                        cmb_proveedores.SelectedValue.ToString(), id_art, cant);

                }
                
                MessageBox.Show("Se registro pedido con exito.");
                this.Close();
            }

        }

        private void Frm_Registrar_Pedido_Load(object sender, EventArgs e)
        {
            DataTable proveedores = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            proveedores = bd.ejecutar_consulta("select Nombre, CUIT from Provedores");

            cmb_proveedores.DataSource = proveedores;
            cmb_proveedores.DisplayMember = "nombre";
            cmb_proveedores.ValueMember = "cuit";


        }

        private void Btn_agregar_Click(object sender, EventArgs e)
        {
            if (txt_cantidad.Text == "")
            {
                MessageBox.Show("No ingreso la cantidad.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                
                int l = dgv_select_articulos.Rows.Count;
                dgv_select_articulos.Rows.Add();
                
                dgv_select_articulos.Rows[l].Cells[0].Value = dgv_articulos.CurrentRow.Cells[0].Value.ToString();
                dgv_select_articulos.Rows[l].Cells[1].Value = dgv_articulos.CurrentRow.Cells[1].Value.ToString();
                dgv_select_articulos.Rows[l].Cells[2].Value = txt_cantidad.Text;


                txt_cantidad.Text = "";
                
            }
        }

        private void articulos_x_proveedor(object sender, EventArgs e)
        {
            DataTable articulos = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            articulos = bd.ejecutar_consulta("exec id_articulo_x_prov '" + cmb_proveedores.SelectedValue.ToString() + "'");

            if (articulos.Rows.Count == 0)
            {
                MessageBox.Show("Este proveedor no contiene articulos, verifique los articulos o registre articulos " +
                    "para ese proveedor.");
            }
            else
            {
                cargar_grilla(articulos);

                cmb_proveedores.Enabled = false;
            }
            
        }

        private void Btn_quitar_Click(object sender, EventArgs e)
        {
            DialogResult result =  MessageBox.Show("¿Seguro que desea quitar este articulo?", 
                "Pregunta!", MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgv_select_articulos.Rows.RemoveAt(dgv_select_articulos.CurrentRow.Index);
            }
        }
    }
}
