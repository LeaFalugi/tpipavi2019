﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Modificar_Pedido : Form
    {
        public string nro_ped { get; set; }
        public string cuit { get; set; }
        public int cant_detalles { get; set; }
        private void recuperar_datos()
        {
            NG_pedidos ng_pedido = new NG_pedidos();
            NG_articulos ng_art = new NG_articulos();
            BE_detallePedido BE_det = new BE_detallePedido();
            DataTable tabla = new DataTable();

            tabla = ng_pedido.recuperar_datos(nro_ped, cuit);

            txt_cuit_prov.Text = cuit; txt_num_ped.Text = nro_ped;
            if (tabla.Rows[0]["fechaEmision"].ToString() != "")
            {
                dtp_fecha_efectiva.Value = DateTime.Parse(tabla.Rows[0]["fechaEmision"].ToString());
            }
            if (tabla.Rows[0]["fechaEfectiva"].ToString() != "")
            {
                dtp_fecha_emision.Value = DateTime.Parse(tabla.Rows[0]["fechaEfectiva"].ToString());
            }

            tabla = BE_det.recuperar_datos(nro_ped, cuit);
            cant_detalles = tabla.Rows.Count;
            dgv_detalles.Rows.Clear();
            for (int i=0; i<cant_detalles; i++)
            {
                dgv_detalles.Rows.Add();

                dgv_detalles.Rows[i].Cells[0].Value = tabla.Rows[i]["nroDetalle"].ToString();
                dgv_detalles.Rows[i].Cells[1].Value = tabla.Rows[i]["idArticulo"].ToString();
                dgv_detalles.Rows[i].Cells[2].Value = tabla.Rows[i]["Nombre"].ToString();
                dgv_detalles.Rows[i].Cells[3].Value = tabla.Rows[i]["cantidad"].ToString();
            }

            tabla = ng_art.articulos_x_proveedor(cuit);

            dgv_articulos.Rows.Clear();
            for (int i=0; i<tabla.Rows.Count; i++)
            {
                dgv_articulos.Rows.Add();
                dgv_articulos.Rows[i].Cells[0].Value = tabla.Rows[i]["idArticulos"].ToString();
                dgv_articulos.Rows[i].Cells[1].Value = tabla.Rows[i]["Nombre"].ToString();

            }
        }
        public Frm_Modificar_Pedido()
        {
            InitializeComponent();
        }

        private void Frm_Modificar_Pedido_Load(object sender, EventArgs e)
        {
            
            recuperar_datos();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            recuperar_datos();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            NG_pedidos ng_ped = new NG_pedidos();
            BE_detallePedido be_det = new BE_detallePedido();
            BE_fecha date = new BE_fecha();

            string fechaEmi, fechaEfec;
            if (dgv_detalles.Rows.Count == 0)
            {
                MessageBox.Show("La listas de detalles del pedido esta vacia.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                fechaEmi = date.convert_fecha(dtp_fecha_emision.Value);
                fechaEfec = date.convert_fecha(dtp_fecha_efectiva.Value);

                ng_ped.modificar_pedido(nro_ped, cuit, fechaEmi, fechaEfec);

                for(int i=0; i<dgv_detalles.Rows.Count; i++)
                {
                    int nro_det = int.Parse(dgv_detalles.Rows[i].Cells[0].Value.ToString());
                    if (nro_det != -1)
                    {
                        be_det.modificar_detalle(nro_det.ToString(), nro_ped, cuit,
                            dgv_detalles.Rows[i].Cells[1].Value.ToString(),
                            dgv_detalles.Rows[i].Cells[3].Value.ToString());
                    }
                    else
                    {
                        be_det.registrar_detalle(nro_ped, cuit,
                            dgv_detalles.Rows[i].Cells[1].Value.ToString(),
                            dgv_detalles.Rows[i].Cells[3].Value.ToString());
                    }
                
                }

                MessageBox.Show("Datos del pedido modificado con exito.");
                this.Close();

            }
        }

        private void Btn_agregar_Click(object sender, EventArgs e)
        {
          
            if (txt_cantidad.Text == "")
            {
                MessageBox.Show("Especifique una cantidad por favor.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                dgv_detalles.Rows.Add();
                int tam = dgv_detalles.Rows.Count;

                dgv_detalles.Rows[tam - 1].Cells[0].Value = -1;
                dgv_detalles.Rows[tam - 1].Cells[1].Value = dgv_articulos.CurrentRow.Cells[0].Value.ToString();
                dgv_detalles.Rows[tam - 1].Cells[2].Value = dgv_articulos.CurrentRow.Cells[1].Value.ToString();
                dgv_detalles.Rows[tam - 1].Cells[3].Value = txt_cantidad.Text;

                txt_cantidad.Text = "";
            }

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_sumar_Click(object sender, EventArgs e)
        {
            int cant, cant2;

            if (txt_cantidad.Text == "")
            {
                MessageBox.Show("Master, capo, capitan, crack, gladiador, jefe, hugo boss, capoeira, crackovia... " +
                    " no me especificaste la cantidad tigre.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                cant = int.Parse(dgv_detalles.CurrentRow.Cells[3].Value.ToString());
                cant2 = int.Parse(txt_cantidad.Text);

                dgv_detalles.CurrentRow.Cells[3].Value = cant + cant2;
                txt_cantidad.Text = "";
            }
        }

        private void Btn_quitar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea quitar este articulo?",
                "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgv_detalles.Rows.RemoveAt(dgv_detalles.CurrentRow.Index);
            }
        }
    }
}
