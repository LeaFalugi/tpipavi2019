﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Usuarios : Form
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_usuarios = new DataTable();

        //Metodos

        private void cargar_grilla_usuarios(DataTable tabla)
        {
            dgv_usuarios.Rows.Clear();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                dgv_usuarios.Rows.Add();
                dgv_usuarios.Rows[i].Cells[0].Value = tabla.Rows[i]["nombreusuario"].ToString();
                dgv_usuarios.Rows[i].Cells[1].Value = tabla.Rows[i]["Nombre"].ToString() + " ";
                dgv_usuarios.Rows[i].Cells[1].Value += tabla.Rows[i]["Apellido"].ToString();
                dgv_usuarios.Rows[i].Cells[2].Value = tabla.Rows[i]["Permiso"].ToString();
                dgv_usuarios.Rows[i].Cells[3].Value = tabla.Rows[i]["DNI"].ToString();
                dgv_usuarios.Rows[i].Cells[4].Value = tabla.Rows[i]["tipoDni"].ToString();
            }
        }

        private void actualizar_grilla()
        {
            tabla_usuarios = bd.ejecutar_consulta("exec grilla_usuarios");
            cargar_grilla_usuarios(tabla_usuarios);
        }
        public Frm_ABM_Usuarios()
        {
            InitializeComponent();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Usuario buscar_usuario = new Frm_Buscar_Usuario();
            buscar_usuario.ShowDialog();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Usuario modificar_usuario = new Frm_Modificar_Usuario();

            modificar_usuario.tipo_documento = dgv_usuarios.CurrentRow.Cells[4].Value.ToString();
            modificar_usuario.n_documento= dgv_usuarios.CurrentRow.Cells[3].Value.ToString();
            modificar_usuario.nombre_usuario= dgv_usuarios.CurrentRow.Cells[0].Value.ToString();

            modificar_usuario.ShowDialog();
            actualizar_grilla();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Frm_ABM_Usuarios_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Usuario registrar_Usuario = new Frm_Registrar_Usuario();
            registrar_Usuario.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_usuarios.CurrentRow == null)
            {
                Console.WriteLine("No seleccionado");
                return;
            }
            string usuarioSelec = dgv_usuarios.CurrentRow.Cells[3].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea eliminar el usuario con DNI: " +
                usuarioSelec + "? ", "Baja Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DataTable tabla = new DataTable();

                Clases.Negocios.NG_Users NG_Usuario = new Clases.Negocios.NG_Users();

                string dni = dgv_usuarios.CurrentRow.Cells[3].Value.ToString();
                string tipoDni = dgv_usuarios.CurrentRow.Cells[4].Value.ToString();
                string username = dgv_usuarios.CurrentRow.Cells[0].Value.ToString();

                tabla = NG_Usuario.recuperar_datos(dni,tipoDni,username);


                NG_Usuario.baja_usuario(dni, tipoDni, tabla.Rows[0]["Contraseña"].ToString());


                MessageBox.Show("Usuario Eliminado", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            actualizar_grilla();
        }

        private void Btn_mostrar_Click(object sender, EventArgs e)
        {
            actualizar_grilla();
        }
    }
}
