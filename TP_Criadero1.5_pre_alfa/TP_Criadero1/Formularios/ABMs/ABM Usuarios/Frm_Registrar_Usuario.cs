﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Registrar_Usuario : Form
    {
        DataTable tabla = new DataTable();
        public Frm_Registrar_Usuario()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Registrar_Usuario_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla2 = new DataTable();

            string sql = "Select CONCAT(nombre,' ',Apellido)as 'nomApe' " +
                "from Empleados where dni not in(" +
                "select e.dni from Empleados e inner join Usuarios u on " +
                "(u.DniEmpleado=e.DNI and u.TipoDNI=e.TipoDNI))";

            string permisos = "select * from permisos";

            tabla2 = bd.ejecutar_consulta(sql);

            cmb_empleados.DataSource = tabla2;
            cmb_empleados.ValueMember = "nomApe";

            tabla2 = bd.ejecutar_consulta(permisos);

            cmb_permiso.DataSource = tabla2;
            cmb_permiso.DisplayMember = "nombre";
            cmb_permiso.ValueMember = "idpermiso";
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            NG_Users usuario = new NG_Users();

            /*Console.WriteLine("Tipo doc:" + cmb_tipo_documento.SelectedIndex + " " +
                cmb_tipo_documento.SelectedValue.ToString());
            Console.WriteLine("Permiso:" + cmb_permiso.SelectedIndex);*/

            if (txt_nombre_usuario.Text==""
                || txt_Contraseña.Text==""
                || cmb_permiso.SelectedIndex== -1)
            {
                MessageBox.Show("Faltan datos por cargar.");
                txt_Contraseña.Focus();
                return;
            }
            else
            {

                usuario.registrar_usuario(tabla.Rows[0]["tipodni"].ToString(),
                    tabla.Rows[0]["dni"].ToString(), txt_nombre_usuario.Text,
                    txt_Contraseña.Text, cmb_permiso.SelectedValue.ToString());
                this.Close();
            }
        }

        private void recuperar_empleado(object sender, EventArgs e)
        {
            NG_empleado ng_emp = new NG_empleado();
            
            //Console.WriteLine("Cmb: index " + cmb_empleados.SelectedIndex + " value "
                //+ cmb_empleados.SelectedValue + " intem " + cmb_empleados.SelectedItem);

            tabla = ng_emp.recuperar_dni_tipo(cmb_empleados.SelectedValue.ToString());
            txt_n_tipo_doc.Text = tabla.Rows[0]["dni"].ToString();
            txt_n_tipo_doc.Text += ", " + tabla.Rows[0]["tipodni"].ToString();
        }
    }
}
