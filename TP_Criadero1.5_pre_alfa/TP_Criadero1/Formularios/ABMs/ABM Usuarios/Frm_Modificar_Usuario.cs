﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Modificar_Usuario : Form
    {
        public string nombre_usuario { get; set; }
        public string tipo_documento { get; set; }
        public string n_documento { get; set; }

        
        private void recuperar_datos()
        {
            NG_Users ng_user = new NG_Users();
            DataTable tabla = new DataTable();

            tabla = ng_user.recuperar_datos(n_documento,tipo_documento,nombre_usuario);

            txt_nombre.Text = nombre_usuario;
            txt_dni.Text = n_documento;
            txt_contraseña.Text = tabla.Rows[0]["contraseña"].ToString();
            cmb_permiso.SelectedValue = int.Parse(tabla.Rows[0]["idPermiso"].ToString());
            cmb_tipo_doc.SelectedValue = int.Parse(tipo_documento);

        }
        public Frm_Modificar_Usuario()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Frm_Modificar_Usuario_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string cons_tipos, cons_permisos;

            cons_tipos = "select * from TiposDNI";
            cons_permisos = "select * from Permisos";

            tabla = bd.ejecutar_consulta(cons_tipos);

            cmb_tipo_doc.DataSource = tabla;
            cmb_tipo_doc.DisplayMember = "Descripcion";
            cmb_tipo_doc.ValueMember = "idTipoDni";

            tabla = bd.ejecutar_consulta(cons_permisos);

            cmb_permiso.DataSource = tabla;
            cmb_permiso.DisplayMember = "nombre";
            cmb_permiso.ValueMember = "idPermiso";

            recuperar_datos();

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            if (chk_mostrar.Checked == true)
            {
                txt_contraseña.UseSystemPasswordChar = true;
            }
            else
            {
                txt_contraseña.UseSystemPasswordChar = false;
            }
        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            NG_Users usuario = new NG_Users();
            if (txt_nombre.Text == ""
                || txt_contraseña.Text == ""
                || txt_dni.Text == ""
                || cmb_tipo_doc.SelectedIndex == -1
                || cmb_permiso.SelectedIndex == -1)
            {
                MessageBox.Show("Faltan datos por cargar.");
            }
            else
            {
                usuario.modificar_usuario(cmb_tipo_doc.SelectedIndex.ToString(),
                    txt_dni.Text, txt_nombre.Text,
                    txt_contraseña.Text, cmb_permiso.SelectedValue.ToString());

                MessageBox.Show("Datos modificados con exito.");
                this.Close();
            }
        }

        private void Chk_mostrar_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
