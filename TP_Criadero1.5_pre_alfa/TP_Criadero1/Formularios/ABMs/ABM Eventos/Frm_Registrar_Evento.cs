﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_Registrar_Evento : Form
    {
        public Frm_Registrar_Evento()
        {
            InitializeComponent();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();
            NG_eventos ng_evento = new NG_eventos();
            string fecha = date.convert_fecha(dtp_fecha_evento.Value);

            if (txt_descripcion.Text==""
                || txt_nombre_evento.Text==""
                || cmb_ciudad.SelectedIndex==-1
                )
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_descripcion.Focus();
            }
            else
            {
                ng_evento.registrar_evento(txt_nombre_evento.Text,
                    fecha, txt_descripcion.Text, cmb_ciudad.SelectedIndex.ToString());

                MessageBox.Show("Se registro el evento con exito.");
                this.Close();

            }
            
        }

        private void Frm_Registrar_Evento_Load(object sender, EventArgs e)
        {
            cmb_ciudad.cargar();
        }
    }
}
