﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Modificar_Evento : Form
    {
        public string id { get; set; }
        private void recuperar_datos()
        {
            NG_eventos ng_evento = new NG_eventos();
            DataTable t_evento = new DataTable();

            t_evento = ng_evento.recuperar_datos(id);

            txt_nom_evento.Text = t_evento.Rows[0]["nombre"].ToString();
            cmb_ciudad.SelectedIndex = int.Parse(t_evento.Rows[0]["idciudad"].ToString());
            rtxt_descripcion.Text = t_evento.Rows[0]["descripcion"].ToString();
            if (t_evento.Rows[0]["fecha"].ToString() != "")
            {
                dtp_fecha_evento.Value = DateTime.Parse(t_evento.Rows[0]["fecha"].ToString());
            }

        }
        public Frm_Modificar_Evento()
        {
            InitializeComponent();
        }

        private void Frm_Modificar_Evento_Load(object sender, EventArgs e)
        {
            DataTable ciudad = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            ciudad = bd.ejecutar_consulta("select * from Ciudad");

            cmb_ciudad.DataSource = ciudad;
            cmb_ciudad.DisplayMember = "nombre";
            cmb_ciudad.ValueMember = "idciudad";

            recuperar_datos();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_restaurar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            BE_fecha date = new BE_fecha();
            NG_eventos ng_evento = new NG_eventos();

            if(txt_nom_evento.Text==""
                || rtxt_descripcion.Text == "")
            {
                MessageBox.Show("Faltan datos obligatorios que cargar (los que tienen un asterisco *).");
                txt_nom_evento.Focus();
                return;
            }
            else
            {
                string fecha = date.convert_fecha(dtp_fecha_evento.Value);

                ng_evento.modificar_evento(id, txt_nom_evento.Text, fecha,
                    rtxt_descripcion.Text, cmb_ciudad.SelectedIndex.ToString());

                MessageBox.Show("Se modificaron los datos con exito.");
                this.Close();
            }
        }
    }
}
