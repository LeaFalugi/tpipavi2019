﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_Eventos : Form
    {
        DataTable tabla_eventos = new DataTable();
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

        public Frm_ABM_Eventos()
        {
            InitializeComponent();
        }

        private void cargar_grilla(DataTable tabla)
        {
            dgv_Eventos.Rows.Clear();

            for (int i=0; i < tabla.Rows.Count; i++)
            {
                dgv_Eventos.Rows.Add();

                dgv_Eventos.Rows[i].Cells[0].Value = tabla.Rows[i]["IdEvento"].ToString();
                dgv_Eventos.Rows[i].Cells[1].Value = tabla.Rows[i]["Nombre"].ToString();
                dgv_Eventos.Rows[i].Cells[2].Value = tabla.Rows[i]["descripcion"].ToString();
                dgv_Eventos.Rows[i].Cells[3].Value = tabla.Rows[i]["fecha"].ToString();
                dgv_Eventos.Rows[i].Cells[4].Value = tabla.Rows[i]["ciudad"].ToString();

            }
        }
        private void actualizar_grilla()
        {
            tabla_eventos = bd.ejecutar_consulta("exec grilla_eventos");
            cargar_grilla(tabla_eventos);
        }
        private void Frm_ABM_Eventos_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Evento buscar_Evento = new Frm_Buscar_Evento();
            buscar_Evento.ShowDialog();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Frm_Modificar_Evento modificar_Evento = new Frm_Modificar_Evento();

            modificar_Evento.id = dgv_Eventos.CurrentRow.Cells[0].Value.ToString();
            modificar_Evento.ShowDialog();
            actualizar_grilla();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Evento registrar_Evento = new Frm_Registrar_Evento();
            registrar_Evento.ShowDialog();
            actualizar_grilla();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (dgv_Eventos.CurrentRow == null)
            {
                return;
            }

            string idEventoSelec = dgv_Eventos.CurrentRow.Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show("¿Esta seguro que desea dar de baja el evento N° " +
                idEventoSelec + "? ", "Baja Evento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                Clases.Negocios.NG_eventos NG_Eventos =new Clases.Negocios.NG_eventos();

                NG_Eventos.baja_Evento(idEventoSelec);

                MessageBox.Show("Evento dado de baja", "Baja", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            actualizar_grilla();
        }
    }
}
