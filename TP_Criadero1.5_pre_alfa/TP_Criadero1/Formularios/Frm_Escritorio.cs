﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


//facu
namespace TP_Criadero1
{
    public partial class Frm_Escritorio : Form
    {
        string usuario = "";
        string clave = "";

        public Frm_Escritorio()
        {
            InitializeComponent();
        }

        private void Frm_Escritorio_Load(object sender, EventArgs e)
        {
            Frm_Login login = new Frm_Login();
            login.ShowDialog();
            usuario = login.usuario;
            clave = login.clave;
            login.Dispose();
            lbl_usuario.Text = "USUARIO: " + usuario;
        }



        //FUNCIONES DE EFECTOS DE LOS BOTONES IMAGENES

        public void efecto_Imagen(PictureBox picB)
        {
            picB.Location = new Point(picB.Location.X + 7, picB.Location.Y - 3);
        }

        public void efecto_Imagen_Salir(PictureBox picB)
        {
            picB.Location = new Point(picB.Location.X - 7, picB.Location.Y + 3);
        }





        //FUNCIÓN PARA ABRIR FORMULARIOS DENTRO DEL PANEL CONTENEDOR

        private void Abrir_Frm_Hijo(Form formHijo)
        {
            if (this.pnl_contenedor.Controls.Count > 0)
            {
                this.pnl_contenedor.Controls.RemoveAt(0);
            }
            formHijo.TopLevel = false;
            //formHijo.Dock = DockStyle.Fill;
            //formHijo.Location = new Point(pnl_contenedor.Height / 2, pnl_contenedor.Width / 2);
            this.pnl_contenedor.Controls.Add(formHijo);
            this.pnl_contenedor.Tag = formHijo;
            formHijo.Show();
        }


        //BOTONES IMAGENES

        private void picB_empleados_Click(object sender, EventArgs e)
        {
            Frm_ABM_Empleado empleados = new Frm_ABM_Empleado();
            Abrir_Frm_Hijo(empleados);
        }

        private void picB_usuarios_Click(object sender, EventArgs e)
        {
            Frm_ABM_Usuarios usuarios = new Frm_ABM_Usuarios();
            Abrir_Frm_Hijo(usuarios);
        }


        private void picB_proveedores_Click(object sender, EventArgs e)
        {
            Frm_ABM_Proveedores proveedores = new Frm_ABM_Proveedores();
            Abrir_Frm_Hijo(proveedores);

        }

        private void picB_pedidos_Click(object sender, EventArgs e)
        {
            Frm_ABM_Pedidos pedidos = new Frm_ABM_Pedidos();
            Abrir_Frm_Hijo(pedidos);
        }

        private void picB_clientes_Click(object sender, EventArgs e)
        {
            Frm_ABM_Cliente clientes = new Frm_ABM_Cliente();
            Abrir_Frm_Hijo(clientes);
        }

        private void picB_perros_Click(object sender, EventArgs e)
        {
            Frm_ABM_Perros perros = new Frm_ABM_Perros();
            Abrir_Frm_Hijo(perros);
        }

        private void picB_eventos_Click(object sender, EventArgs e)
        {
            Frm_ABM_Eventos eventos = new Frm_ABM_Eventos();
            Abrir_Frm_Hijo(eventos);
        }

        private void picB_articulos_Click(object sender, EventArgs e)
        {
            Frm_ABM_articulos articulos = new Frm_ABM_articulos();
            Abrir_Frm_Hijo(articulos);
        }



        //MENÚS
        private void Men_Ver_Articulo_Click(object sender, EventArgs e)
        {
            Frm_ABM_articulos articulos = new Frm_ABM_articulos();
            Abrir_Frm_Hijo(articulos);
        }

        private void Men_Buscar_Articulo_Click(object sender, EventArgs e)
        {
            Frm_buscar_articulo buscar_Articulo = new Frm_buscar_articulo();
            buscar_Articulo.ShowDialog();
            buscar_Articulo.Dispose();
        }

        private void Men_Registrar_Articulo_Click(object sender, EventArgs e)
        {
            Frm_registrar_arcticulo registrar_Arcticulo = new Frm_registrar_arcticulo();
            registrar_Arcticulo.ShowDialog();
            registrar_Arcticulo.Dispose();
        }


        private void Men_Ver_Cliente_Click(object sender, EventArgs e)
        {
            Frm_ABM_Cliente clientes = new Frm_ABM_Cliente();
            Abrir_Frm_Hijo(clientes);
        }

        private void Men_Buscar_Cliente_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Cliente buscar_Cliente = new Frm_Buscar_Cliente();
            buscar_Cliente.ShowDialog();
            buscar_Cliente.Dispose();
        }

        private void Men_Registrar_Cliente_Click(object sender, EventArgs e)
        {
            Frm_registrar_cliente registrar_Cliente = new Frm_registrar_cliente();
            registrar_Cliente.ShowDialog();
            registrar_Cliente.Dispose();
        }

        private void Men_Ver_Empleado_Click(object sender, EventArgs e)
        {
            Frm_ABM_Empleado empleados = new Frm_ABM_Empleado();
            Abrir_Frm_Hijo(empleados);
        }

        private void Men_Buscar_Empleado_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Empleado buscar_Empleado = new Frm_Buscar_Empleado();
            buscar_Empleado.ShowDialog();
            buscar_Empleado.Dispose();
        }

        private void Men_Registrar_Empleado_Click(object sender, EventArgs e)
        {
            Frm_registrar_empleado registrar_Empleado = new Frm_registrar_empleado();
            registrar_Empleado.ShowDialog();
            registrar_Empleado.Dispose();
        }

        private void Men_Ver_Evento_Click(object sender, EventArgs e)
        {
            Frm_ABM_Eventos eventos = new Frm_ABM_Eventos();
            Abrir_Frm_Hijo(eventos);
        }

        private void Men_Buscar_Evento_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Evento buscar_Evento = new Frm_Buscar_Evento();
            buscar_Evento.ShowDialog();
            buscar_Evento.Dispose();
        }

        private void Men_Registrar_Evento_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Evento registrar_Evento = new Frm_Registrar_Evento();
            registrar_Evento.ShowDialog();
            registrar_Evento.Dispose();
        }

        private void Men_Ver_Perro_Click(object sender, EventArgs e)
        {
            Frm_ABM_Perros perros = new Frm_ABM_Perros();
            Abrir_Frm_Hijo(perros);
        }

        private void Men_Buscar_Perro_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Perro buscar_Perro = new Frm_Buscar_Perro();
            buscar_Perro.ShowDialog();
            buscar_Perro.Dispose();
        }

        private void Men_Registrar_Perro_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Perro registrar_Perro = new Frm_Registrar_Perro();
            registrar_Perro.ShowDialog();
            registrar_Perro.Dispose();
        }

        private void Men_Ver_Proveedor_Click(object sender, EventArgs e)
        {
            Frm_ABM_Proveedores proveedores = new Frm_ABM_Proveedores();
            Abrir_Frm_Hijo(proveedores);
        }

        private void Men_Buscar_Proveedor_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Proveedor buscar_Proveedor = new Frm_Buscar_Proveedor();
            buscar_Proveedor.ShowDialog();
            buscar_Proveedor.Dispose();
        }

        private void Men_Registrar_Proveedor_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Proveedor registrar_Proveedor = new Frm_Registrar_Proveedor();
            registrar_Proveedor.ShowDialog();
            registrar_Proveedor.Dispose();
        }

        private void Men_Ver_Pedido_Click(object sender, EventArgs e)
        {
            Frm_ABM_Pedidos pedidos = new Frm_ABM_Pedidos();
            Abrir_Frm_Hijo(pedidos);
        }


        private void Men_Buscar_Pedido_Click(object sender, EventArgs e)
        {
            Frm_buscar_Pedido buscar_Pedido = new Frm_buscar_Pedido();
            buscar_Pedido.ShowDialog();
            buscar_Pedido.Dispose();
        }

        private void Men_Registar_Pedido_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Pedido registrar_Pedido = new Frm_Registrar_Pedido();
            registrar_Pedido.ShowDialog();
            registrar_Pedido.Dispose();
        }

        private void Men_Ver_Usuario_Click(object sender, EventArgs e)
        {
            Frm_ABM_Usuarios usuarios = new Frm_ABM_Usuarios();
            Abrir_Frm_Hijo(usuarios);
        }

        private void Men_Buscar_Usuario_Click(object sender, EventArgs e)
        {
            Frm_Buscar_Usuario buscar_Usuario = new Frm_Buscar_Usuario();
            buscar_Usuario.ShowDialog();
            buscar_Usuario.Dispose();
        }

        private void Men_Registrar_Usuario_Click(object sender, EventArgs e)
        {
            Frm_Registrar_Usuario registrar_Usuario = new Frm_Registrar_Usuario();
            registrar_Usuario.ShowDialog();
            registrar_Usuario.Dispose();
        }

        private void Men_Salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }








        //EFECTO IMAGENES
        private void picB_empleados_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_empleados);
        }

        private void picB_empleados_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_empleados);
        }

        private void picB_clientes_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_clientes);
        }

        private void picB_clientes_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_clientes);
        }

        private void picB_perros_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_perros);
        }

        private void picB_perros_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_perros);
        }

        private void picB_eventos_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_eventos);
        }

        private void picB_eventos_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_eventos);
        }

        private void picB_proveedores_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_proveedores);
        }

        private void picB_proveedores_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_proveedores);
        }

        private void picB_pedidos_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_pedidos);
        }

        private void picB_pedidos_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_pedidos);
        }

        private void picB_usuarios_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_usuarios);
        }

        private void picB_usuarios_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_usuarios);
        }

        private void picB_articulos_MouseEnter(object sender, EventArgs e)
        {
            efecto_Imagen(picB_articulos);
        }

        private void picB_articulos_MouseLeave(object sender, EventArgs e)
        {
            efecto_Imagen_Salir(picB_articulos);
        }

        private void CambiarUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Login login = new Frm_Login();

            login.ShowDialog();
        }

        private void RegistrarVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_ABM_factura facturas = new Frm_ABM_factura();

            facturas.ShowDialog();
        }

        private void RegistrarIngresoDeMercaderiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_ABM_ing_mercaderia ingreso_mercaderia = new Frm_ABM_ing_mercaderia();

            ingreso_mercaderia.ShowDialog();
        }

        private void Btn_reg_vta_Click(object sender, EventArgs e)
        {
            Frm_registrar_factura registrar_factura = new Frm_registrar_factura();

            registrar_factura.ShowDialog();
        }

        private void Btn_ing_mercaderia_Click(object sender, EventArgs e)
        {
            Frm_registrar_ing_mercaderia ingreso_mercaderia = new Frm_registrar_ing_mercaderia();

            ingreso_mercaderia.ShowDialog();
        }

        private void ventasPorMesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_reportBiuer report = new Frm_reportBiuer();

            report.ShowDialog();
        }

        private void estadisticaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_estadistiCas estadistica = new Frm_estadistiCas();

            estadistica.ShowDialog();
        }
    }
}