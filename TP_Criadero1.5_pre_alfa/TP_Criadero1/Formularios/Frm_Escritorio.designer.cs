﻿namespace TP_Criadero1
{
    partial class Frm_Escritorio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Escritorio));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gererarInformeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasPorMesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.articulosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Articulo = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Articulo = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Articulo = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Cliente = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Cliente = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Cliente = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Empleado = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Empleado = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Empleado = new System.Windows.Forms.ToolStripMenuItem();
            this.eventosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Evento = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Evento = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Evento = new System.Windows.Forms.ToolStripMenuItem();
            this.perrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Perro = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Perro = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Perro = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Proveedor = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Proveedor = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Proveedor = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Pedido = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Pedido = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registar_Pedido = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Ver_Usuario = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Buscar_Usuario = new System.Windows.Forms.ToolStripMenuItem();
            this.Men_Registrar_Usuario = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarVentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarIngresoDeMercaderiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnl_botones = new System.Windows.Forms.Panel();
            this.btn_ing_mercaderia = new System.Windows.Forms.Button();
            this.btn_reg_vta = new System.Windows.Forms.Button();
            this.picB_articulos = new System.Windows.Forms.PictureBox();
            this.picB_usuarios = new System.Windows.Forms.PictureBox();
            this.picB_pedidos = new System.Windows.Forms.PictureBox();
            this.picB_proveedores = new System.Windows.Forms.PictureBox();
            this.picB_eventos = new System.Windows.Forms.PictureBox();
            this.picB_perros = new System.Windows.Forms.PictureBox();
            this.picB_clientes = new System.Windows.Forms.PictureBox();
            this.picB_empleados = new System.Windows.Forms.PictureBox();
            this.pnl_estado = new System.Windows.Forms.Panel();
            this.lbl_usuario = new System.Windows.Forms.Label();
            this.pnl_contenedor = new System.Windows.Forms.Panel();
            this.estadisticaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.pnl_botones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picB_articulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_usuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_pedidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_proveedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_eventos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_perros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_clientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_empleados)).BeginInit();
            this.pnl_estado.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.aBMsToolStripMenuItem,
            this.consultasToolStripMenuItem,
            this.opcionesToolStripMenuItem,
            this.editarToolStripMenuItem,
            this.verToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(964, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gererarInformeToolStripMenuItem,
            this.Men_Salir});
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.abrirToolStripMenuItem.Text = "Archivo";
            // 
            // gererarInformeToolStripMenuItem
            // 
            this.gererarInformeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasPorMesToolStripMenuItem,
            this.estadisticaToolStripMenuItem});
            this.gererarInformeToolStripMenuItem.Name = "gererarInformeToolStripMenuItem";
            this.gererarInformeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gererarInformeToolStripMenuItem.Text = "Gererar informe";
            // 
            // ventasPorMesToolStripMenuItem
            // 
            this.ventasPorMesToolStripMenuItem.Name = "ventasPorMesToolStripMenuItem";
            this.ventasPorMesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ventasPorMesToolStripMenuItem.Text = "Ventas por mes";
            this.ventasPorMesToolStripMenuItem.Click += new System.EventHandler(this.ventasPorMesToolStripMenuItem_Click);
            // 
            // Men_Salir
            // 
            this.Men_Salir.Name = "Men_Salir";
            this.Men_Salir.Size = new System.Drawing.Size(180, 22);
            this.Men_Salir.Text = "Salir";
            this.Men_Salir.Click += new System.EventHandler(this.Men_Salir_Click);
            // 
            // aBMsToolStripMenuItem
            // 
            this.aBMsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.articulosToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.empleadosToolStripMenuItem,
            this.eventosToolStripMenuItem,
            this.perrosToolStripMenuItem,
            this.proveedoresToolStripMenuItem,
            this.pedidosToolStripMenuItem,
            this.usuariosToolStripMenuItem});
            this.aBMsToolStripMenuItem.Name = "aBMsToolStripMenuItem";
            this.aBMsToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.aBMsToolStripMenuItem.Text = "ABMs";
            // 
            // articulosToolStripMenuItem
            // 
            this.articulosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Articulo,
            this.Men_Buscar_Articulo,
            this.Men_Registrar_Articulo});
            this.articulosToolStripMenuItem.Name = "articulosToolStripMenuItem";
            this.articulosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.articulosToolStripMenuItem.Text = "Articulos";
            // 
            // Men_Ver_Articulo
            // 
            this.Men_Ver_Articulo.Name = "Men_Ver_Articulo";
            this.Men_Ver_Articulo.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Articulo.Text = "Ver";
            this.Men_Ver_Articulo.Click += new System.EventHandler(this.Men_Ver_Articulo_Click);
            // 
            // Men_Buscar_Articulo
            // 
            this.Men_Buscar_Articulo.Name = "Men_Buscar_Articulo";
            this.Men_Buscar_Articulo.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Articulo.Text = "Buscar";
            this.Men_Buscar_Articulo.Click += new System.EventHandler(this.Men_Buscar_Articulo_Click);
            // 
            // Men_Registrar_Articulo
            // 
            this.Men_Registrar_Articulo.Name = "Men_Registrar_Articulo";
            this.Men_Registrar_Articulo.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Articulo.Text = "Registrar";
            this.Men_Registrar_Articulo.Click += new System.EventHandler(this.Men_Registrar_Articulo_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Cliente,
            this.Men_Buscar_Cliente,
            this.Men_Registrar_Cliente});
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // Men_Ver_Cliente
            // 
            this.Men_Ver_Cliente.Name = "Men_Ver_Cliente";
            this.Men_Ver_Cliente.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Cliente.Text = "Ver";
            this.Men_Ver_Cliente.Click += new System.EventHandler(this.Men_Ver_Cliente_Click);
            // 
            // Men_Buscar_Cliente
            // 
            this.Men_Buscar_Cliente.Name = "Men_Buscar_Cliente";
            this.Men_Buscar_Cliente.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Cliente.Text = "Buscar";
            this.Men_Buscar_Cliente.Click += new System.EventHandler(this.Men_Buscar_Cliente_Click);
            // 
            // Men_Registrar_Cliente
            // 
            this.Men_Registrar_Cliente.Name = "Men_Registrar_Cliente";
            this.Men_Registrar_Cliente.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Cliente.Text = "Registrar";
            this.Men_Registrar_Cliente.Click += new System.EventHandler(this.Men_Registrar_Cliente_Click);
            // 
            // empleadosToolStripMenuItem
            // 
            this.empleadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Empleado,
            this.Men_Buscar_Empleado,
            this.Men_Registrar_Empleado});
            this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
            this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.empleadosToolStripMenuItem.Text = "Empleados";
            // 
            // Men_Ver_Empleado
            // 
            this.Men_Ver_Empleado.Name = "Men_Ver_Empleado";
            this.Men_Ver_Empleado.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Empleado.Text = "Ver";
            this.Men_Ver_Empleado.Click += new System.EventHandler(this.Men_Ver_Empleado_Click);
            // 
            // Men_Buscar_Empleado
            // 
            this.Men_Buscar_Empleado.Name = "Men_Buscar_Empleado";
            this.Men_Buscar_Empleado.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Empleado.Text = "Buscar";
            this.Men_Buscar_Empleado.Click += new System.EventHandler(this.Men_Buscar_Empleado_Click);
            // 
            // Men_Registrar_Empleado
            // 
            this.Men_Registrar_Empleado.Name = "Men_Registrar_Empleado";
            this.Men_Registrar_Empleado.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Empleado.Text = "Registrar";
            this.Men_Registrar_Empleado.Click += new System.EventHandler(this.Men_Registrar_Empleado_Click);
            // 
            // eventosToolStripMenuItem
            // 
            this.eventosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Evento,
            this.Men_Buscar_Evento,
            this.Men_Registrar_Evento});
            this.eventosToolStripMenuItem.Name = "eventosToolStripMenuItem";
            this.eventosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.eventosToolStripMenuItem.Text = "Eventos";
            // 
            // Men_Ver_Evento
            // 
            this.Men_Ver_Evento.Name = "Men_Ver_Evento";
            this.Men_Ver_Evento.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Evento.Text = "Ver";
            this.Men_Ver_Evento.Click += new System.EventHandler(this.Men_Ver_Evento_Click);
            // 
            // Men_Buscar_Evento
            // 
            this.Men_Buscar_Evento.Name = "Men_Buscar_Evento";
            this.Men_Buscar_Evento.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Evento.Text = "Buscar";
            this.Men_Buscar_Evento.Click += new System.EventHandler(this.Men_Buscar_Evento_Click);
            // 
            // Men_Registrar_Evento
            // 
            this.Men_Registrar_Evento.Name = "Men_Registrar_Evento";
            this.Men_Registrar_Evento.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Evento.Text = "Registrar";
            this.Men_Registrar_Evento.Click += new System.EventHandler(this.Men_Registrar_Evento_Click);
            // 
            // perrosToolStripMenuItem
            // 
            this.perrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Perro,
            this.Men_Buscar_Perro,
            this.Men_Registrar_Perro});
            this.perrosToolStripMenuItem.Name = "perrosToolStripMenuItem";
            this.perrosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.perrosToolStripMenuItem.Text = "Perros";
            // 
            // Men_Ver_Perro
            // 
            this.Men_Ver_Perro.Name = "Men_Ver_Perro";
            this.Men_Ver_Perro.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Perro.Text = "Ver";
            this.Men_Ver_Perro.Click += new System.EventHandler(this.Men_Ver_Perro_Click);
            // 
            // Men_Buscar_Perro
            // 
            this.Men_Buscar_Perro.Name = "Men_Buscar_Perro";
            this.Men_Buscar_Perro.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Perro.Text = "Buscar";
            this.Men_Buscar_Perro.Click += new System.EventHandler(this.Men_Buscar_Perro_Click);
            // 
            // Men_Registrar_Perro
            // 
            this.Men_Registrar_Perro.Name = "Men_Registrar_Perro";
            this.Men_Registrar_Perro.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Perro.Text = "Registrar";
            this.Men_Registrar_Perro.Click += new System.EventHandler(this.Men_Registrar_Perro_Click);
            // 
            // proveedoresToolStripMenuItem
            // 
            this.proveedoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Proveedor,
            this.Men_Buscar_Proveedor,
            this.Men_Registrar_Proveedor});
            this.proveedoresToolStripMenuItem.Name = "proveedoresToolStripMenuItem";
            this.proveedoresToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.proveedoresToolStripMenuItem.Text = "Proveedores";
            // 
            // Men_Ver_Proveedor
            // 
            this.Men_Ver_Proveedor.Name = "Men_Ver_Proveedor";
            this.Men_Ver_Proveedor.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Proveedor.Text = "Ver";
            this.Men_Ver_Proveedor.Click += new System.EventHandler(this.Men_Ver_Proveedor_Click);
            // 
            // Men_Buscar_Proveedor
            // 
            this.Men_Buscar_Proveedor.Name = "Men_Buscar_Proveedor";
            this.Men_Buscar_Proveedor.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Proveedor.Text = "Buscar";
            this.Men_Buscar_Proveedor.Click += new System.EventHandler(this.Men_Buscar_Proveedor_Click);
            // 
            // Men_Registrar_Proveedor
            // 
            this.Men_Registrar_Proveedor.Name = "Men_Registrar_Proveedor";
            this.Men_Registrar_Proveedor.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Proveedor.Text = "Registrar";
            this.Men_Registrar_Proveedor.Click += new System.EventHandler(this.Men_Registrar_Proveedor_Click);
            // 
            // pedidosToolStripMenuItem
            // 
            this.pedidosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Pedido,
            this.Men_Buscar_Pedido,
            this.Men_Registar_Pedido});
            this.pedidosToolStripMenuItem.Name = "pedidosToolStripMenuItem";
            this.pedidosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.pedidosToolStripMenuItem.Text = "Pedidos";
            // 
            // Men_Ver_Pedido
            // 
            this.Men_Ver_Pedido.Name = "Men_Ver_Pedido";
            this.Men_Ver_Pedido.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Pedido.Text = "Ver";
            this.Men_Ver_Pedido.Click += new System.EventHandler(this.Men_Ver_Pedido_Click);
            // 
            // Men_Buscar_Pedido
            // 
            this.Men_Buscar_Pedido.Name = "Men_Buscar_Pedido";
            this.Men_Buscar_Pedido.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Pedido.Text = "Buscar";
            this.Men_Buscar_Pedido.Click += new System.EventHandler(this.Men_Buscar_Pedido_Click);
            // 
            // Men_Registar_Pedido
            // 
            this.Men_Registar_Pedido.Name = "Men_Registar_Pedido";
            this.Men_Registar_Pedido.Size = new System.Drawing.Size(120, 22);
            this.Men_Registar_Pedido.Text = "Registrar";
            this.Men_Registar_Pedido.Click += new System.EventHandler(this.Men_Registar_Pedido_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Men_Ver_Usuario,
            this.Men_Buscar_Usuario,
            this.Men_Registrar_Usuario});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // Men_Ver_Usuario
            // 
            this.Men_Ver_Usuario.Name = "Men_Ver_Usuario";
            this.Men_Ver_Usuario.Size = new System.Drawing.Size(120, 22);
            this.Men_Ver_Usuario.Text = "Ver";
            this.Men_Ver_Usuario.Click += new System.EventHandler(this.Men_Ver_Usuario_Click);
            // 
            // Men_Buscar_Usuario
            // 
            this.Men_Buscar_Usuario.Name = "Men_Buscar_Usuario";
            this.Men_Buscar_Usuario.Size = new System.Drawing.Size(120, 22);
            this.Men_Buscar_Usuario.Text = "Buscar";
            this.Men_Buscar_Usuario.Click += new System.EventHandler(this.Men_Buscar_Usuario_Click);
            // 
            // Men_Registrar_Usuario
            // 
            this.Men_Registrar_Usuario.Name = "Men_Registrar_Usuario";
            this.Men_Registrar_Usuario.Size = new System.Drawing.Size(120, 22);
            this.Men_Registrar_Usuario.Text = "Registrar";
            this.Men_Registrar_Usuario.Click += new System.EventHandler(this.Men_Registrar_Usuario_Click);
            // 
            // consultasToolStripMenuItem
            // 
            this.consultasToolStripMenuItem.Name = "consultasToolStripMenuItem";
            this.consultasToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.consultasToolStripMenuItem.Text = "Consultas";
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarUsuarioToolStripMenuItem,
            this.registrarVentaToolStripMenuItem,
            this.registrarIngresoDeMercaderiaToolStripMenuItem});
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.opcionesToolStripMenuItem.Text = "Opciones";
            // 
            // cambiarUsuarioToolStripMenuItem
            // 
            this.cambiarUsuarioToolStripMenuItem.Name = "cambiarUsuarioToolStripMenuItem";
            this.cambiarUsuarioToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.cambiarUsuarioToolStripMenuItem.Text = "Cambiar Usuario";
            this.cambiarUsuarioToolStripMenuItem.Click += new System.EventHandler(this.CambiarUsuarioToolStripMenuItem_Click);
            // 
            // registrarVentaToolStripMenuItem
            // 
            this.registrarVentaToolStripMenuItem.Name = "registrarVentaToolStripMenuItem";
            this.registrarVentaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.registrarVentaToolStripMenuItem.Text = "Ventas/Facturas";
            this.registrarVentaToolStripMenuItem.Click += new System.EventHandler(this.RegistrarVentaToolStripMenuItem_Click);
            // 
            // registrarIngresoDeMercaderiaToolStripMenuItem
            // 
            this.registrarIngresoDeMercaderiaToolStripMenuItem.Name = "registrarIngresoDeMercaderiaToolStripMenuItem";
            this.registrarIngresoDeMercaderiaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.registrarIngresoDeMercaderiaToolStripMenuItem.Text = "Pedidos Recibidos";
            this.registrarIngresoDeMercaderiaToolStripMenuItem.Click += new System.EventHandler(this.RegistrarIngresoDeMercaderiaToolStripMenuItem_Click);
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.editarToolStripMenuItem.Text = "Editar";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // pnl_botones
            // 
            this.pnl_botones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnl_botones.AutoScroll = true;
            this.pnl_botones.AutoScrollMargin = new System.Drawing.Size(2, 2);
            this.pnl_botones.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pnl_botones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_botones.Controls.Add(this.btn_ing_mercaderia);
            this.pnl_botones.Controls.Add(this.btn_reg_vta);
            this.pnl_botones.Controls.Add(this.picB_articulos);
            this.pnl_botones.Controls.Add(this.picB_usuarios);
            this.pnl_botones.Controls.Add(this.picB_pedidos);
            this.pnl_botones.Controls.Add(this.picB_proveedores);
            this.pnl_botones.Controls.Add(this.picB_eventos);
            this.pnl_botones.Controls.Add(this.picB_perros);
            this.pnl_botones.Controls.Add(this.picB_clientes);
            this.pnl_botones.Controls.Add(this.picB_empleados);
            this.pnl_botones.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnl_botones.Location = new System.Drawing.Point(0, 24);
            this.pnl_botones.Name = "pnl_botones";
            this.pnl_botones.Size = new System.Drawing.Size(180, 567);
            this.pnl_botones.TabIndex = 1;
            // 
            // btn_ing_mercaderia
            // 
            this.btn_ing_mercaderia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ing_mercaderia.Location = new System.Drawing.Point(84, 529);
            this.btn_ing_mercaderia.Name = "btn_ing_mercaderia";
            this.btn_ing_mercaderia.Size = new System.Drawing.Size(75, 23);
            this.btn_ing_mercaderia.TabIndex = 9;
            this.btn_ing_mercaderia.Text = "Ing. Merc.";
            this.btn_ing_mercaderia.UseVisualStyleBackColor = true;
            this.btn_ing_mercaderia.Click += new System.EventHandler(this.Btn_ing_mercaderia_Click);
            // 
            // btn_reg_vta
            // 
            this.btn_reg_vta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_reg_vta.Location = new System.Drawing.Point(3, 529);
            this.btn_reg_vta.Name = "btn_reg_vta";
            this.btn_reg_vta.Size = new System.Drawing.Size(75, 23);
            this.btn_reg_vta.TabIndex = 8;
            this.btn_reg_vta.Text = "Reg. Venta";
            this.btn_reg_vta.UseVisualStyleBackColor = true;
            this.btn_reg_vta.Click += new System.EventHandler(this.Btn_reg_vta_Click);
            // 
            // picB_articulos
            // 
            this.picB_articulos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_articulos.BackgroundImage = global::TP_Criadero1.Properties.Resources.Articulos;
            this.picB_articulos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_articulos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_articulos.Location = new System.Drawing.Point(11, 116);
            this.picB_articulos.Name = "picB_articulos";
            this.picB_articulos.Size = new System.Drawing.Size(140, 43);
            this.picB_articulos.TabIndex = 7;
            this.picB_articulos.TabStop = false;
            this.picB_articulos.Click += new System.EventHandler(this.picB_articulos_Click);
            this.picB_articulos.MouseEnter += new System.EventHandler(this.picB_articulos_MouseEnter);
            this.picB_articulos.MouseLeave += new System.EventHandler(this.picB_articulos_MouseLeave);
            // 
            // picB_usuarios
            // 
            this.picB_usuarios.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_usuarios.BackgroundImage = global::TP_Criadero1.Properties.Resources.Usuarios;
            this.picB_usuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_usuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_usuarios.Location = new System.Drawing.Point(11, 459);
            this.picB_usuarios.Name = "picB_usuarios";
            this.picB_usuarios.Size = new System.Drawing.Size(140, 43);
            this.picB_usuarios.TabIndex = 6;
            this.picB_usuarios.TabStop = false;
            this.picB_usuarios.Click += new System.EventHandler(this.picB_usuarios_Click);
            this.picB_usuarios.MouseEnter += new System.EventHandler(this.picB_usuarios_MouseEnter);
            this.picB_usuarios.MouseLeave += new System.EventHandler(this.picB_usuarios_MouseLeave);
            // 
            // picB_pedidos
            // 
            this.picB_pedidos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_pedidos.BackgroundImage = global::TP_Criadero1.Properties.Resources.Pedidos;
            this.picB_pedidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_pedidos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_pedidos.Location = new System.Drawing.Point(11, 410);
            this.picB_pedidos.Name = "picB_pedidos";
            this.picB_pedidos.Size = new System.Drawing.Size(140, 43);
            this.picB_pedidos.TabIndex = 5;
            this.picB_pedidos.TabStop = false;
            this.picB_pedidos.Click += new System.EventHandler(this.picB_pedidos_Click);
            this.picB_pedidos.MouseEnter += new System.EventHandler(this.picB_pedidos_MouseEnter);
            this.picB_pedidos.MouseLeave += new System.EventHandler(this.picB_pedidos_MouseLeave);
            // 
            // picB_proveedores
            // 
            this.picB_proveedores.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_proveedores.BackgroundImage = global::TP_Criadero1.Properties.Resources.proveedores;
            this.picB_proveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_proveedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_proveedores.Location = new System.Drawing.Point(11, 361);
            this.picB_proveedores.Name = "picB_proveedores";
            this.picB_proveedores.Size = new System.Drawing.Size(140, 43);
            this.picB_proveedores.TabIndex = 4;
            this.picB_proveedores.TabStop = false;
            this.picB_proveedores.Click += new System.EventHandler(this.picB_proveedores_Click);
            this.picB_proveedores.MouseEnter += new System.EventHandler(this.picB_proveedores_MouseEnter);
            this.picB_proveedores.MouseLeave += new System.EventHandler(this.picB_proveedores_MouseLeave);
            // 
            // picB_eventos
            // 
            this.picB_eventos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_eventos.BackgroundImage = global::TP_Criadero1.Properties.Resources.Eventos;
            this.picB_eventos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_eventos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_eventos.Location = new System.Drawing.Point(11, 263);
            this.picB_eventos.Name = "picB_eventos";
            this.picB_eventos.Size = new System.Drawing.Size(140, 43);
            this.picB_eventos.TabIndex = 3;
            this.picB_eventos.TabStop = false;
            this.picB_eventos.Click += new System.EventHandler(this.picB_eventos_Click);
            this.picB_eventos.MouseEnter += new System.EventHandler(this.picB_eventos_MouseEnter);
            this.picB_eventos.MouseLeave += new System.EventHandler(this.picB_eventos_MouseLeave);
            // 
            // picB_perros
            // 
            this.picB_perros.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_perros.BackgroundImage = global::TP_Criadero1.Properties.Resources.Perros;
            this.picB_perros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_perros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_perros.Location = new System.Drawing.Point(11, 312);
            this.picB_perros.Name = "picB_perros";
            this.picB_perros.Size = new System.Drawing.Size(140, 43);
            this.picB_perros.TabIndex = 2;
            this.picB_perros.TabStop = false;
            this.picB_perros.Click += new System.EventHandler(this.picB_perros_Click);
            this.picB_perros.MouseEnter += new System.EventHandler(this.picB_perros_MouseEnter);
            this.picB_perros.MouseLeave += new System.EventHandler(this.picB_perros_MouseLeave);
            // 
            // picB_clientes
            // 
            this.picB_clientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_clientes.BackgroundImage = global::TP_Criadero1.Properties.Resources.Clientes;
            this.picB_clientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_clientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_clientes.Location = new System.Drawing.Point(11, 165);
            this.picB_clientes.Name = "picB_clientes";
            this.picB_clientes.Size = new System.Drawing.Size(140, 43);
            this.picB_clientes.TabIndex = 1;
            this.picB_clientes.TabStop = false;
            this.picB_clientes.Click += new System.EventHandler(this.picB_clientes_Click);
            this.picB_clientes.MouseEnter += new System.EventHandler(this.picB_clientes_MouseEnter);
            this.picB_clientes.MouseLeave += new System.EventHandler(this.picB_clientes_MouseLeave);
            // 
            // picB_empleados
            // 
            this.picB_empleados.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picB_empleados.BackgroundImage = global::TP_Criadero1.Properties.Resources.Empleados;
            this.picB_empleados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picB_empleados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB_empleados.Location = new System.Drawing.Point(11, 214);
            this.picB_empleados.Name = "picB_empleados";
            this.picB_empleados.Size = new System.Drawing.Size(140, 43);
            this.picB_empleados.TabIndex = 0;
            this.picB_empleados.TabStop = false;
            this.picB_empleados.Click += new System.EventHandler(this.picB_empleados_Click);
            this.picB_empleados.MouseEnter += new System.EventHandler(this.picB_empleados_MouseEnter);
            this.picB_empleados.MouseLeave += new System.EventHandler(this.picB_empleados_MouseLeave);
            // 
            // pnl_estado
            // 
            this.pnl_estado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_estado.AutoScroll = true;
            this.pnl_estado.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.pnl_estado.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pnl_estado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_estado.Controls.Add(this.lbl_usuario);
            this.pnl_estado.Location = new System.Drawing.Point(0, 583);
            this.pnl_estado.Name = "pnl_estado";
            this.pnl_estado.Size = new System.Drawing.Size(959, 32);
            this.pnl_estado.TabIndex = 3;
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_usuario.Location = new System.Drawing.Point(658, 9);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(296, 14);
            this.lbl_usuario.TabIndex = 0;
            this.lbl_usuario.Text = "USUARIO: <>";
            this.lbl_usuario.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pnl_contenedor
            // 
            this.pnl_contenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_contenedor.AutoScroll = true;
            this.pnl_contenedor.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.pnl_contenedor.Location = new System.Drawing.Point(186, 27);
            this.pnl_contenedor.Name = "pnl_contenedor";
            this.pnl_contenedor.Size = new System.Drawing.Size(773, 556);
            this.pnl_contenedor.TabIndex = 4;
            // 
            // estadisticaToolStripMenuItem
            // 
            this.estadisticaToolStripMenuItem.Name = "estadisticaToolStripMenuItem";
            this.estadisticaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.estadisticaToolStripMenuItem.Text = "Estadistica";
            this.estadisticaToolStripMenuItem.Click += new System.EventHandler(this.estadisticaToolStripMenuItem_Click);
            // 
            // Frm_Escritorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(964, 620);
            this.Controls.Add(this.pnl_contenedor);
            this.Controls.Add(this.pnl_estado);
            this.Controls.Add(this.pnl_botones);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Frm_Escritorio";
            this.Text = "Criadero";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Escritorio_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnl_botones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picB_articulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_usuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_pedidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_proveedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_eventos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_perros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_clientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB_empleados)).EndInit();
            this.pnl_estado.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel pnl_botones;
        private System.Windows.Forms.Panel pnl_estado;
        private System.Windows.Forms.Label lbl_usuario;
        private System.Windows.Forms.PictureBox picB_empleados;
        private System.Windows.Forms.PictureBox picB_clientes;
        private System.Windows.Forms.PictureBox picB_perros;
        private System.Windows.Forms.PictureBox picB_eventos;
        private System.Windows.Forms.PictureBox picB_pedidos;
        private System.Windows.Forms.PictureBox picB_proveedores;
        private System.Windows.Forms.PictureBox picB_usuarios;
        private System.Windows.Forms.PictureBox picB_articulos;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Empleado;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Empleado;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Empleado;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Cliente;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Cliente;
        private System.Windows.Forms.ToolStripMenuItem perrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Perro;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Perro;
        private System.Windows.Forms.ToolStripMenuItem eventosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Evento;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Evento;
        private System.Windows.Forms.ToolStripMenuItem proveedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Proveedor;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Proveedor;
        private System.Windows.Forms.ToolStripMenuItem articulosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Articulo;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Articulo;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Articulo;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Cliente;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Evento;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Perro;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Proveedor;
        private System.Windows.Forms.ToolStripMenuItem pedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Pedido;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Pedido;
        private System.Windows.Forms.ToolStripMenuItem Men_Registar_Pedido;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Men_Ver_Usuario;
        private System.Windows.Forms.ToolStripMenuItem Men_Buscar_Usuario;
        private System.Windows.Forms.ToolStripMenuItem Men_Registrar_Usuario;
        private System.Windows.Forms.ToolStripMenuItem Men_Salir;
        private System.Windows.Forms.ToolStripMenuItem consultasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.Panel pnl_contenedor;
        private System.Windows.Forms.ToolStripMenuItem cambiarUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarVentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarIngresoDeMercaderiaToolStripMenuItem;
        private System.Windows.Forms.Button btn_ing_mercaderia;
        private System.Windows.Forms.Button btn_reg_vta;
        private System.Windows.Forms.ToolStripMenuItem gererarInformeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventasPorMesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estadisticaToolStripMenuItem;
    }
}

