﻿namespace TP_Criadero1
{
    partial class Frm_Buscar_factura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chk_tipo = new System.Windows.Forms.CheckBox();
            this.cmb_tipo = new System.Windows.Forms.ComboBox();
            this.chk_fecha = new System.Windows.Forms.CheckBox();
            this.dtp_fecha_emision = new System.Windows.Forms.DateTimePicker();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.chk_cliente = new System.Windows.Forms.CheckBox();
            this.txt_n_cliente = new System.Windows.Forms.TextBox();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chk_tipo
            // 
            this.chk_tipo.AutoSize = true;
            this.chk_tipo.Location = new System.Drawing.Point(246, 131);
            this.chk_tipo.Name = "chk_tipo";
            this.chk_tipo.Size = new System.Drawing.Size(43, 17);
            this.chk_tipo.TabIndex = 14;
            this.chk_tipo.Text = "tipo";
            this.chk_tipo.UseVisualStyleBackColor = true;
            this.chk_tipo.CheckedChanged += new System.EventHandler(this.Chk_tipo_CheckedChanged_1);
            // 
            // cmb_tipo
            // 
            this.cmb_tipo.Enabled = false;
            this.cmb_tipo.FormattingEnabled = true;
            this.cmb_tipo.Location = new System.Drawing.Point(45, 129);
            this.cmb_tipo.Name = "cmb_tipo";
            this.cmb_tipo.Size = new System.Drawing.Size(195, 21);
            this.cmb_tipo.TabIndex = 13;
            // 
            // chk_fecha
            // 
            this.chk_fecha.AutoSize = true;
            this.chk_fecha.Location = new System.Drawing.Point(246, 82);
            this.chk_fecha.Name = "chk_fecha";
            this.chk_fecha.Size = new System.Drawing.Size(53, 17);
            this.chk_fecha.TabIndex = 12;
            this.chk_fecha.Text = "fecha";
            this.chk_fecha.UseVisualStyleBackColor = true;
            this.chk_fecha.CheckedChanged += new System.EventHandler(this.Chk_fecha_CheckedChanged_1);
            // 
            // dtp_fecha_emision
            // 
            this.dtp_fecha_emision.Enabled = false;
            this.dtp_fecha_emision.Location = new System.Drawing.Point(45, 82);
            this.dtp_fecha_emision.Name = "dtp_fecha_emision";
            this.dtp_fecha_emision.Size = new System.Drawing.Size(195, 20);
            this.dtp_fecha_emision.TabIndex = 11;
            // 
            // btn_buscar
            // 
            this.btn_buscar.Location = new System.Drawing.Point(83, 192);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 10;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.Btn_buscar_Click);
            // 
            // chk_cliente
            // 
            this.chk_cliente.AutoSize = true;
            this.chk_cliente.Location = new System.Drawing.Point(246, 38);
            this.chk_cliente.Name = "chk_cliente";
            this.chk_cliente.Size = new System.Drawing.Size(58, 17);
            this.chk_cliente.TabIndex = 9;
            this.chk_cliente.Text = "Cliente";
            this.chk_cliente.UseVisualStyleBackColor = true;
            this.chk_cliente.CheckedChanged += new System.EventHandler(this.Chk_cliente_CheckedChanged);
            // 
            // txt_n_cliente
            // 
            this.txt_n_cliente.Enabled = false;
            this.txt_n_cliente.Location = new System.Drawing.Point(45, 36);
            this.txt_n_cliente.Name = "txt_n_cliente";
            this.txt_n_cliente.Size = new System.Drawing.Size(195, 20);
            this.txt_n_cliente.TabIndex = 8;
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(164, 192);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 15;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // Frm_Buscar_factura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(330, 257);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.chk_tipo);
            this.Controls.Add(this.cmb_tipo);
            this.Controls.Add(this.chk_fecha);
            this.Controls.Add(this.dtp_fecha_emision);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.chk_cliente);
            this.Controls.Add(this.txt_n_cliente);
            this.Name = "Frm_Buscar_factura";
            this.Text = "Frm_Buscar_factura";
            this.Load += new System.EventHandler(this.Frm_Buscar_factura_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chk_tipo;
        private System.Windows.Forms.ComboBox cmb_tipo;
        private System.Windows.Forms.CheckBox chk_fecha;
        private System.Windows.Forms.DateTimePicker dtp_fecha_emision;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.CheckBox chk_cliente;
        private System.Windows.Forms.TextBox txt_n_cliente;
        private System.Windows.Forms.Button btn_cancelar;
    }
}