﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_registrar_ing_mercaderia : Form
    {
        public string nro_pedido { get; set; }
        public string cuit { get; set; }
        private void recuperar_datos()
        {
            NG_pedidos ng_ped = new NG_pedidos();
            DataTable tabla = new DataTable();

        }
        private void cargar_grilla_detalles(DataTable tabla)
        {
            dgv_detalles.Rows.Clear();

            for(int i=0; i < tabla.Rows.Count; i++)
            {
                dgv_detalles.Rows.Add();

                dgv_detalles.Rows[i].Cells[0].Value = tabla.Rows[i]["nroDetalle"].ToString();
                dgv_detalles.Rows[i].Cells[1].Value = tabla.Rows[i]["idArticulo"].ToString();
                dgv_detalles.Rows[i].Cells[2].Value = tabla.Rows[i]["nombre"].ToString();
                dgv_detalles.Rows[i].Cells[3].Value = tabla.Rows[i]["cantidad"].ToString();
                dgv_detalles.Rows[i].Cells[4].Value = int.Parse(tabla.Rows[i]["cantidad"].ToString())
                    * float.Parse(tabla.Rows[i]["precioUnitario"].ToString());

            }
        }
        private void cargar_grilla_pedidos(DataTable tabla)
        {
            dgv_pedidos.Rows.Clear();

            for (int i=0; i<tabla.Rows.Count; i++)
            {
                dgv_pedidos.Rows.Add();

                dgv_pedidos.Rows[i].Cells[0].Value = tabla.Rows[i]["nro"].ToString();
                dgv_pedidos.Rows[i].Cells[1].Value = tabla.Rows[i]["id_provedor"].ToString();
                dgv_pedidos.Rows[i].Cells[2].Value = tabla.Rows[i]["FechaEmision"].ToString();

            }

        }
        public Frm_registrar_ing_mercaderia()
        {
            InitializeComponent();
        }

        private void Frm_registrar_ing_mercaderia_Load(object sender, EventArgs e)
        {
            DataTable proveedores = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            proveedores = bd.ejecutar_consulta("select Nombre, CUIT from Provedores");

            cmb_proveedores.DataSource = proveedores;
            cmb_proveedores.DisplayMember = "nombre";
            cmb_proveedores.ValueMember = "cuit";
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {

            if(dgv_detalles.Rows.Count==0|| dgv_pedidos.Rows.Count == 0)
            {
                MessageBox.Show("No selecionaste nada...");
                txt_num_ped.Focus();
                return;
            }
            else
            {
                NG_pedidos ng_ped = new NG_pedidos();
                NG_articulos ng_art = new NG_articulos();
                BE_fecha date = new BE_fecha();

                DialogResult result = MessageBox.Show("¿Seguro que desea registrar la venta?" + "\n" +
                "Esto implica modificar stocks permanentemente y no podra modificar los datos posteriormente.",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    for (int i=0; i < dgv_detalles.Rows.Count; i++)
                    {
                        ng_art.transaccion_articulos(dgv_detalles.Rows[i].Cells[1].Value.ToString(),
                            dgv_detalles.Rows[i].Cells[3].Value.ToString());
                    }
                    string fecha = date.convert_fecha(DateTime.Now);
                    ng_ped.transaccion_pedido(txt_num_ped.Text, cmb_proveedores.SelectedValue.ToString(),
                        fecha);

                    MessageBox.Show("Se registro el ingreso con exito.");
                    this.Close();
                }

            }
            
        }

        private void pedidos_x_proveedor(object sender, EventArgs e)
        {
            DataTable pedidos = new DataTable();
            NG_pedidos ng_ped = new NG_pedidos();

            Console.WriteLine("cmb_proveedores: " + cmb_proveedores.SelectedValue.ToString());
            pedidos = ng_ped.pedidos_x_proveedor(cmb_proveedores.SelectedValue.ToString());

            if (pedidos.Rows.Count == 0)
            {
                MessageBox.Show("Este proveedor no tiene pedidos realizados o sin recibir, para mi que se re confundion el chabon, batio cualquiera.");

            }
            else
            {
                cargar_grilla_pedidos(pedidos);
            }

        }

        private void detalles_x_pedido(object sender, DataGridViewCellMouseEventArgs e)
        {
            BE_detallePedido be_det = new BE_detallePedido();
            DataTable detalles = new DataTable();

            Console.WriteLine("nro pedido: " + dgv_pedidos.CurrentRow.Cells[0].Value.ToString());
            Console.WriteLine("cuit: " + dgv_pedidos.CurrentRow.Cells[1].Value.ToString());
            detalles = be_det.detalles_x_pedido(dgv_pedidos.CurrentRow.Cells[0].Value.ToString(),
                dgv_pedidos.CurrentRow.Cells[1].Value.ToString());
            txt_num_ped.Text = dgv_pedidos.CurrentRow.Cells[0].Value.ToString();

            cargar_grilla_detalles(detalles);

            float sum = 0;
            for (int i = 0; i < dgv_detalles.Rows.Count; i++)
            {
                sum += float.Parse(dgv_detalles.Rows[i].Cells[4].Value.ToString());
            }
            txt_total.Text = "$" + sum;

        }
    }
}
