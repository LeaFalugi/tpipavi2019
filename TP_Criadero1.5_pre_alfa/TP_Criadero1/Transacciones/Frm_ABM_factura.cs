﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1;

namespace TP_Criadero1
{
    public partial class Frm_ABM_factura : Form
    {
        private void cargar_grilla(DataTable tabla)
        {
            dgv_facturas.Rows.Clear();

            for (int i=0; i<tabla.Rows.Count; i++)
            {
                dgv_facturas.Rows.Add();

                dgv_facturas.Rows[i].Cells[0].Value = tabla.Rows[i]["nroFactura"].ToString();
                dgv_facturas.Rows[i].Cells[1].Value = tabla.Rows[i]["Articulo"].ToString();
                dgv_facturas.Rows[i].Cells[2].Value = tabla.Rows[i]["Cantidad"].ToString();

                dgv_facturas.Rows[i].Cells[3].Value = tabla.Rows[i]["NomCliente"].ToString();
                dgv_facturas.Rows[i].Cells[3].Value += " " + tabla.Rows[i]["apeCliente"].ToString();

                dgv_facturas.Rows[i].Cells[4].Value = tabla.Rows[i]["nomEmpleado"].ToString();
                dgv_facturas.Rows[i].Cells[4].Value += " " + tabla.Rows[i]["apeEmpleado"].ToString();

                dgv_facturas.Rows[i].Cells[5].Value = tabla.Rows[i]["tipoFactura"].ToString();
                dgv_facturas.Rows[i].Cells[6].Value = tabla.Rows[i]["fechaEmision"].ToString();

            }

        }
        private void actualizar_grilla()
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = bd.ejecutar_consulta("exec grilla_facturas");

            cargar_grilla(tabla);
        }
        public Frm_ABM_factura()
        {
            InitializeComponent();
        }

        private void Frm_ABM_factura_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_registrar_factura registar_factura = new Frm_registrar_factura();

            registar_factura.ShowDialog();
            actualizar_grilla();
        }

        private void Btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_buscar_Click(object sender, EventArgs e)
        {
            Frm_Buscar_factura buscar_factura = new Frm_Buscar_factura();
            DataTable tabla = new DataTable();

            buscar_factura.ShowDialog();
            if (buscar_factura.cancelado==false)
            {
                cargar_grilla(buscar_factura.Tabla_busqueda);
            }
            
            buscar_factura.Dispose();
        }
    }
}
