﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1
{
    public partial class Frm_registrar_factura : Form
    {
        private void cargar_disponibles(DataTable tabla)
        {
            dgv_disponibles.Rows.Clear();

            for (int i=0; i<tabla.Rows.Count; i++)
            {
                dgv_disponibles.Rows.Add();

                dgv_disponibles.Rows[i].Cells[0].Value = tabla.Rows[i]["idArticulos"].ToString();
                dgv_disponibles.Rows[i].Cells[1].Value = tabla.Rows[i]["nombre"].ToString();
                dgv_disponibles.Rows[i].Cells[3].Value = tabla.Rows[i]["disponibilidad"].ToString();
                dgv_disponibles.Rows[i].Cells[2].Value = tabla.Rows[i]["precioUnitario"].ToString();

            }
        }
        private void cargar_clientes(DataTable tabla)
        {
            dgv_clientes.Rows.Clear();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                dgv_clientes.Rows.Add();

                dgv_clientes.Rows[i].Cells[0].Value = tabla.Rows[i]["Nombre"].ToString() + " ";
                dgv_clientes.Rows[i].Cells[0].Value += tabla.Rows[i]["apellido"].ToString();

                dgv_clientes.Rows[i].Cells[1].Value = tabla.Rows[i]["dni"].ToString();
                dgv_clientes.Rows[i].Cells[2].Value = tabla.Rows[i]["tipoDni"].ToString();

            }
        }

        public Frm_registrar_factura()
        {
            InitializeComponent();
        }

        private void GroupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void Frm_registrar_factura_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string cons_tiposDni = "select * from tiposDni";
            string cons_tiposFactu = "select * from tiposFacturas";

            tabla = bd.ejecutar_consulta(cons_tiposDni);

            cmb_tipo_doc.DataSource = tabla;
            cmb_tipo_doc.DisplayMember = "Descripcion";
            cmb_tipo_doc.ValueMember = "idtipodni";

            tabla = bd.ejecutar_consulta(cons_tiposFactu);

            cmb_tipo_fact.DataSource = tabla;
            cmb_tipo_fact.DisplayMember = "descripcion";
            cmb_tipo_fact.ValueMember = "idtipofactura";

            tabla = bd.ejecutar_consulta("select IdArticulos,Nombre,disponibilidad,precioUnitario " +
                "from Articulos " +
                "where disponibilidad>0");

            cargar_disponibles(tabla);

        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            NG_facturas ng_fact = new NG_facturas();
            BE_fecha date = new BE_fecha();

            if (txt_doc_empl.Text==""
                ||cmb_tipo_doc.SelectedIndex==-1
                ||txt_n_cliente.Text==""
                ||cmb_tipo_fact.SelectedIndex==-1
                || dgv_seleccionados.Rows.Count == 0)
            {
                MessageBox.Show("Faltan datos por cargar, aquellos con un * (asterisco) son obligatorios.");
                txt_n_cliente.Focus();
                return;
            }
            else
            {
                DialogResult result = MessageBox.Show("¿Seguro que desea registrar la venta?"+"\n" +
                "Esto implica modificar stocks permanentemente y no podra modificar los datos posteriormente.",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string fecha = date.convert_fecha(DateTime.Now);

                    ng_fact.registrar_factura(cmb_tipo_fact.SelectedValue.ToString(),
                        txt_doc_empl.Text, cmb_tipo_doc.SelectedValue.ToString(),
                        txt_result_cliente.Text, txt_tipo_doc_cli.Text, fecha);
                    for (int i=0; i < dgv_seleccionados.Rows.Count; i++)
                    {
                        ng_fact.registrar_det_factura(dgv_seleccionados.Rows[i].Cells[0].Value.ToString(),
                            dgv_seleccionados.Rows[i].Cells[2].Value.ToString());
                    }

                    MessageBox.Show("Se registro la venta con exito.");
                    this.Close();
                }

            }
            
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_buscar_Click(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            if (txt_n_cliente.Text == "")
            {
                MessageBox.Show("No ingreso nombre ni apellido...");
                txt_n_cliente.Focus();
                return;
            }
            else
            {
                string sql = "select Nombre, Apellido, DNI, TipoDNI from Clientes where Nombre like '" +
                    txt_n_cliente.Text + "%'";

                tabla = bd.ejecutar_consulta(sql);
                cargar_clientes(tabla);
                panel1.Visible = true;
            }
        }

        private void Dgv_clientes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = dgv_clientes.CurrentCell.RowIndex;

            Console.WriteLine("indice celda: " + index);

            txt_result_cliente.Text =dgv_clientes.Rows[index].Cells[1].Value.ToString();
            txt_tipo_doc_cli.Text = dgv_clientes.Rows[index].Cells[2].Value.ToString();
            txt_n_cliente.Text = dgv_clientes.Rows[index].Cells[0].Value.ToString();

            panel1.Visible = false;
        }

        private void Btn_agregar_Click(object sender, EventArgs e)
        {
            if (txt_cantidad.Text == "")
            {
                MessageBox.Show("Especifique una cantidad.");
                txt_cantidad.Focus();
                return;
            }
            else
            {
                if (int.Parse(txt_cantidad.Text) > int.Parse(dgv_disponibles.CurrentRow.Cells[2].Value.ToString()))
                {
                    MessageBox.Show("La cantidad solicitada es mayor a la cantidad disponible.");
                    txt_cantidad.Focus();
                    return;
                }
                else
                {
                    int tam = dgv_seleccionados.Rows.Count;
                    float precio = int.Parse(dgv_disponibles.CurrentRow.Cells[2].Value.ToString());
                    dgv_seleccionados.Rows.Add();

                    dgv_seleccionados.Rows[tam].Cells[0].Value = dgv_disponibles.CurrentRow.Cells[0].Value;
                    dgv_seleccionados.Rows[tam].Cells[1].Value = dgv_disponibles.CurrentRow.Cells[1].Value;
                    dgv_seleccionados.Rows[tam].Cells[2].Value = int.Parse(txt_cantidad.Text);
                    dgv_seleccionados.Rows[tam].Cells[3].Value = int.Parse(txt_cantidad.Text) * precio;

                    int replace = int.Parse(dgv_disponibles.CurrentRow.Cells[3].Value.ToString()) - int.Parse(txt_cantidad.Text);
                    dgv_disponibles.CurrentRow.Cells[3].Value = replace.ToString();

                    int sum = 0;
                    for (int i = 0; i < dgv_seleccionados.Rows.Count; i++)
                    {
                        sum += int.Parse(dgv_seleccionados.Rows[i].Cells[3].Value.ToString());
                    }

                    txt_total.Text = "$" + sum;
                    txt_cantidad.Text = "";
                }
            }
        }

        private void Btn_quitar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea quitar este articulo?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                int cant = int.Parse(dgv_seleccionados.CurrentRow.Cells[2].Value.ToString());

                dgv_seleccionados.Rows.RemoveAt(dgv_seleccionados.CurrentRow.Index);

                int replace = int.Parse(dgv_disponibles.CurrentRow.Cells[3].Value.ToString()) + cant;
                dgv_disponibles.CurrentRow.Cells[3].Value = replace.ToString();

                btn_agregar.Enabled = true;
                
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
