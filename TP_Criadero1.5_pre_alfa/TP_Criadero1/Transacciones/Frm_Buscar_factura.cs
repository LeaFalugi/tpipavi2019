﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases.Negocios;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_Buscar_factura : Form
    {
        public DataTable Tabla_busqueda { get; set; }
        public bool cancelado { get; set; }
        public Frm_Buscar_factura()
        {
            InitializeComponent();
        }

        private void Frm_Buscar_factura_Load(object sender, EventArgs e)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();
            cancelado = true;

            string sql = "select * from tiposFacturas";

            tabla = bd.ejecutar_consulta(sql);

            cmb_tipo.DataSource = tabla;
            cmb_tipo.DisplayMember = "Descripcion";
            cmb_tipo.ValueMember = "idTipoFactura";
        }

        private void Chk_cliente_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_cliente.Checked)
            {
                txt_n_cliente.Enabled = true;
                cmb_tipo.Enabled = false;
                dtp_fecha_emision.Enabled = false;
                chk_tipo.Checked = false;
                chk_fecha.Checked = false;
            }
            else
            {
                txt_n_cliente.Enabled = false;
            }
        }

        private void Chk_fecha_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chk_fecha.Checked)
            {
                dtp_fecha_emision.Enabled = true;
                txt_n_cliente.Enabled = false;
                cmb_tipo.Enabled = false;
                chk_cliente.Checked = false;
                chk_tipo.Checked = false;
            }
            else
            {
                dtp_fecha_emision.Enabled = false;
            }
        }

        private void Chk_tipo_CheckedChanged_1(object sender, EventArgs e)
        {

            if (chk_tipo.Checked)
            {
                cmb_tipo.Enabled = true;
                txt_n_cliente.Enabled = false;
                dtp_fecha_emision.Enabled = false;
                chk_cliente.Checked = false;
                chk_fecha.Checked = false;
            }
            else
            {
                cmb_tipo.Enabled = false;
            }
        }

        private void Btn_buscar_Click(object sender, EventArgs e)
        {
            NG_facturas ng_fac = new NG_facturas();
            DataTable busqueda = new DataTable();
            BE_fecha date = new BE_fecha();
            string buscar_x = "";

            if (chk_cliente.Checked)
            {
                buscar_x = " where CONCAT(c.nombre, c.apellido) like '%" + txt_n_cliente.Text + "%'";
                busqueda = ng_fac.buscar_facturas(buscar_x);
            }
            if (chk_tipo.Checked)
            {
                buscar_x = " where f.IdTipoFactura = " + cmb_tipo.SelectedValue.ToString();
                busqueda = ng_fac.buscar_facturas(buscar_x);
            }
            if (chk_fecha.Checked)
            {
                string fecha = date.convert_fecha(dtp_fecha_emision.Value);
                buscar_x = "where f.FechaEmision='" + fecha + "'";
                busqueda = ng_fac.buscar_facturas(buscar_x);
            }
            else
            {
                MessageBox.Show("No seleccionaste nada.");
                txt_n_cliente.Focus();
                return;
            }

            cancelado = false;
            Tabla_busqueda = busqueda;
            this.Close();
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                  "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
