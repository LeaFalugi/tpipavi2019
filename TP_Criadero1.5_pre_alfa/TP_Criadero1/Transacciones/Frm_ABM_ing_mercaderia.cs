﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1
{
    public partial class Frm_ABM_ing_mercaderia : Form
    {
        private void cargar_grilla(DataTable tabla)
        {
            dgv_pedidos.Rows.Clear();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                dgv_pedidos.Rows.Add();

                dgv_pedidos.Rows[i].Cells[0].Value = tabla.Rows[i]["nro"].ToString();
                dgv_pedidos.Rows[i].Cells[1].Value = tabla.Rows[i]["Articulo"].ToString();
                dgv_pedidos.Rows[i].Cells[2].Value = tabla.Rows[i]["cantidad"].ToString();
                dgv_pedidos.Rows[i].Cells[3].Value = tabla.Rows[i]["proveedor"].ToString();
                dgv_pedidos.Rows[i].Cells[4].Value = tabla.Rows[i]["fechaemision"].ToString();
                dgv_pedidos.Rows[i].Cells[5].Value = tabla.Rows[i]["fechaefectiva"].ToString();
                dgv_pedidos.Rows[i].Cells[6].Value = tabla.Rows[i]["id_Provedor"].ToString();

            }
        }
        private void actualizar_grilla()
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            cargar_grilla(bd.ejecutar_consulta("exec pedidos_recibidos"));
        }
        public Frm_ABM_ing_mercaderia()
        {
            InitializeComponent();
        }

        private void Frm_ABM_ing_mercaderia_Load(object sender, EventArgs e)
        {
            actualizar_grilla();
        }

        private void Btn_cerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Seguro que desea cerrar el proceso?",
                   "Pregunta!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void Btn_registrar_Click(object sender, EventArgs e)
        {
            Frm_registrar_ing_mercaderia reg_ingreso = new Frm_registrar_ing_mercaderia();

            reg_ingreso.ShowDialog();
            actualizar_grilla();
        }
    }
}
