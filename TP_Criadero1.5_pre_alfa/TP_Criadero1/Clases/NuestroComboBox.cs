﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_Criadero1.Clases;

namespace TP_Criadero1.Clases
{
    class NuestroComboBox: ComboBox
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        public string nombre_tabla { get; set; }
        public string pk { get; set; }
        public string descriptor { get; set; }
        public bool validar { get; set; }
        public void cargar()
        {
            string sql = "SELECT * FROM " + this.nombre_tabla.Trim();
            this.DisplayMember = descriptor;
            this.ValueMember = pk;
            this.DataSource = bd.ejecutar_consulta(sql);
        }
        public void cargar(string nom_tabla, string _pk, string _descriptor)
        {
            string sql = "SELECT * FROM " + nom_tabla.Trim();
            this.DisplayMember = _descriptor;
            this.ValueMember = _pk;
            this.DataSource = bd.ejecutar_consulta(sql);
        }
    }
}
