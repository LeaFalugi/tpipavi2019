﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace TP_Criadero1.Clases
{
    //DEL CRIADERO
    class BE_conexion_criadero_BD
    {
        public enum estado_BE { correcto, error }
        public enum tipo_conexion { simple, transaccion }

        OleDbConnection conexion = new OleDbConnection();
        OleDbCommand cmd = new OleDbCommand();
        OleDbTransaction transaccion;
        tipo_conexion tipo_conex = tipo_conexion.simple;
        estado_BE control_transac = estado_BE.correcto;
        string cadena_conexion = "Provider=SQLNCLI11;Data Source=LAPTOP-STE2N382\\FACUSQL;Integrated Security=SSPI;Initial Catalog=Criadero";
        public void inicio_transaccion()
        {
            tipo_conex = tipo_conexion.transaccion;
            control_transac = estado_BE.correcto;
        }
        public estado_BE cerrar_transaccion()
        {
            if (control_transac == estado_BE.correcto)
            {
                transaccion.Commit();
            }
            else
            {
                transaccion.Rollback();
            }

            tipo_conex = tipo_conexion.simple;
            desconectar();
            return control_transac;

        }
        private void conectar()
        {
            if (conexion.State == ConnectionState.Closed)
            {
                conexion.ConnectionString = cadena_conexion;
                conexion.Open();

                cmd.Connection = conexion;
                cmd.CommandType = CommandType.Text;

                if (tipo_conex == tipo_conexion.transaccion)
                {
                    transaccion = conexion.BeginTransaction(IsolationLevel.ReadUncommitted);
                    cmd.Transaction = transaccion;
                }
            }
        }

        private void desconectar()
        {
            if (tipo_conex == tipo_conexion.simple)
            {
                conexion.Close();
                control_transac = estado_BE.correcto;
            }
        }

        public DataTable ejecutar_consulta(string consulta_eseCuEle)
        {
            DataTable tabla = new DataTable();

            conectar();

            cmd.CommandText = consulta_eseCuEle;

            try
            {
                tabla.Load(cmd.ExecuteReader());
            }
            catch(Exception e)
            {
                control_transac = estado_BE.error;
                MessageBox.Show("Error en la base de datos." + "\n"
                    + "Comando: " + consulta_eseCuEle + "\n" +
                    "Mensaje de error: " + e.Message);

            }

            desconectar();
            return tabla;
        }

        public estado_BE ejecutar_procedimiento(string consulta_sql)
        {
            conectar();

            cmd.CommandText = consulta_sql;

            try
            {
                cmd.ExecuteNonQuery();
            }catch(Exception e)
            {
                control_transac = estado_BE.error;

                MessageBox.Show("Error en la base de datos." + "\n"
                    + "Comando: " + consulta_sql + "\n" +
                    "Mensaje de error: " + e.Message);
            }

            desconectar();
            return control_transac;
        }
    }
}
