﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_Criadero1.Clases;
using System.Data;

namespace TP_Criadero1.Clases
{
    class BE_Direcciones
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        public void modificar_direccion(string id,string calle, string nro, string idBarrio)
        {
            string sql = "exec modificar_direccion ";
            sql += id + ",'" + calle + "'," + nro + "," + idBarrio;

            bd.ejecutar_procedimiento(sql);
        }
        public DataTable recuperar_datos(string id)
        {
            DataTable tabla = new DataTable();

            string sql = "select calle, nro, IdBarrio from Direcciones where IdDireccion=" + id;

            tabla = bd.ejecutar_consulta(sql);
            return tabla;
        }
        public string registrar_direccion(string calle, int idBarrio, string nro)
        {
            //idBarrio = 1;
            if (nro == "" || calle == "")
            {
                return "null";
            }
            else
            {

                int id = verificar_direccion(calle, nro);
                if (id > 0)
                {
                    return id.ToString();
                }
                else
                {
                    string proc = "exec registrar_direccion '" + calle.Trim() +
                    "' , " + nro + "," + idBarrio;
                    Console.WriteLine("exec: " + proc);

                    bd.ejecutar_procedimiento(proc);

                    return verificar_direccion(calle, nro).ToString();

                }
            }

        }

        private int verificar_direccion(string calle, string nro)
        {
            DataTable tabla = new DataTable();

            string consulta = " select idDireccion from direcciones " +
                "where calle = '" + calle.Trim() + "' and nro= " + nro;
            Console.WriteLine("Direccion: " + consulta);
            tabla = bd.ejecutar_consulta(consulta);

            if (tabla.Rows.Count == 0)
            {
                return -1;
            }
            else
            {
                return int.Parse(tabla.Rows[0]["iddireccion"].ToString());
            }

        }
    }
}
