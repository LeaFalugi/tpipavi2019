﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TP_Criadero1.Clases
{
    class BE_detallePedido
    {
        public DataTable recuperar_datos(string nroPed, string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select dp.nroDetalle,dp.idArticulo,a.Nombre ,dp.cantidad " +
                "from DetallePedido dp join Articulos a on a.IdArticulos=dp.idArticulo " +
                "where dp.nroPedido=" + nroPed + " and dp.idProveedor='" + cuit + "'";
            //Console.WriteLine("Recuperar datos- detalle: " + sql);
            tabla = bd.ejecutar_consulta(sql);

            return tabla;
        }
        public DataTable detalles_x_pedido(string nroPed, string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select dp.nroDetalle, dp.idArticulo, a.Nombre, dp.cantidad, a.precioUnitario ";

            sql += "from DetallePedido dp join Articulos a on a.IdArticulos=dp.idArticulo ";
            sql += "where nroPedido = " + nroPed + " and idProveedor='" + cuit + "'";

            Console.WriteLine("detalles x pedido: " + sql);
            tabla = bd.ejecutar_consulta(sql);
            return tabla;
        }
        public void registrar_detalle(string nroPedido, string cuitProv
            ,string idArticulo, string cantidad)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_det_pedido ";

            sql += nroPedido + ",'" + cuitProv + "',";
            sql += idArticulo + "," + cantidad;

            Console.WriteLine("Reg_det: " + sql);

            bd.ejecutar_procedimiento(sql);
        }

        public void modificar_detalle(string nro_detalle, string nro_pedido,
            string cuit, string id_art, string cantidad)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_det_pedido ";

            sql += nro_detalle + "," + nro_pedido + ",'" + cuit + "'," + id_art + "," + cantidad;

            Console.WriteLine("Modif_detalle: " + sql);

            bd.ejecutar_procedimiento(sql);

        }
    }
}
