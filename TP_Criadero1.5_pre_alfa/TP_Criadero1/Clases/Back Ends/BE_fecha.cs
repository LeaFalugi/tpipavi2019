﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TP_Criadero1.Clases
{
    class BE_fecha
    {
        public string convert_fecha(DateTime date)
        {
            string fecha = date.Year.ToString() + "-" + date.Month.ToString() + "-" + date.Day.ToString();

            return fecha;
        }

    }
}
