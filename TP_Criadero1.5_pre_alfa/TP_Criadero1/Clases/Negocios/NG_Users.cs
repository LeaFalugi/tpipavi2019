﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;


namespace TP_Criadero1.Clases.Negocios
{
    class NG_Users
    {
        public enum respuesta { correcto, error }
        public respuesta validar_usuario(string usuario, string pssw)
        {
            BE_conexion_criadero_BD _BD = new BE_conexion_criadero_BD();
            string sql = "";

            sql = @"SELECT * 
                    FROM Usuarios 
                    WHERE NombreUsuario = '" + usuario.Trim() + "'"
                    + " AND Contraseña = '" + pssw.Trim() + "'";

            DataTable tabla = new DataTable();

            tabla = _BD.ejecutar_consulta(sql);

            if (tabla.Rows.Count == 0)
            {
                return respuesta.error;
            }
            else
            {
                return respuesta.correcto;
            }
        }
        public int recuperar_Dni(string usuario, string pssw)
        {
            string sql = "";
            sql = @"SELECT * 
                    FROM Usuarios 
                    WHERE NombreUsuario = '" + usuario.Trim() + "'"
                    + " AND Contraseña = '" + pssw.Trim() + "'";

            DataTable tabla = new DataTable();
            BE_conexion_criadero_BD _BD = new BE_conexion_criadero_BD();
            tabla = _BD.ejecutar_consulta(sql);
            return int.Parse(tabla.Rows[0]["DniEmpleado"].ToString());
          
        }
        
        public void registrar_usuario(string tipoDni, string dni, string username
            ,string contraseña, string permiso)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_usuario '" + username + "'," + dni + ",";
            sql += tipoDni + ",'" + contraseña + "'," + permiso;
            bd.ejecutar_procedimiento(sql);
        }

        public DataTable recuperar_datos(string dni,string tipoDoc, string username)
        {
            string sql = "select Contraseña, IdPermiso from Usuarios ";
            sql += "where DniEmpleado=" + dni + " and TipoDNI=" + tipoDoc +
                " and NombreUsuario='" + username + "'";
            DataTable tabla = new DataTable();
            BE_conexion_criadero_BD _BD = new BE_conexion_criadero_BD();

            tabla = _BD.ejecutar_consulta(sql);
            return tabla;
        }

        public void baja_usuario(string dni, string tipoDni, string contraseña)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec baja_usuarios " + dni + "," + tipoDni + "," + contraseña;

            bd.ejecutar_procedimiento(sql);

        }

        public void modificar_usuario(string tipoDni, string dni, string username
            , string contraseña, string permiso)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_usuario '" + username + "'," + dni + ",";
            sql += tipoDni + ",'" + contraseña + "'," + permiso;

            bd.ejecutar_procedimiento(sql);
        }
        }
}
