﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_articulos
    {
        public void transaccion_articulos(string id, string  cant)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec agregar_stock " + id + "," + cant;
            bd.inicio_transaccion();
            bd.ejecutar_procedimiento(sql);
            bd.cerrar_transaccion();

        }
        public DataTable articulos_x_proveedor(string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select IdArticulos, Nombre from Articulos where IdProvedor='" + cuit + "'";

            tabla = bd.ejecutar_consulta(sql);

            return tabla;
        }
        
        public int recuperar_id(string nombre, string descripcion)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            string sql = "select idArticulos " +
                "from articulos where nombre = '" + nombre + "'" +
                " and descripcion = '" + descripcion + "'";

            DataTable tabla = new DataTable();

            tabla = bd.ejecutar_consulta(sql);
            return int.Parse(tabla.Rows[0]["idArticulos"].ToString());
        }

        public void registrar_articulo(string nombre,
            string precio, string cuit_prov, string descripcion="null")
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_articulo '";

            sql += nombre.Trim() + "','" + descripcion.Trim() + "'";
            sql += ", " + precio.Trim() + ",'" + cuit_prov + "'";

            bd.ejecutar_procedimiento(sql);
        }
        public void modificar_articulo(string id, string nombre, string descripcion,
                string precio, string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_articulo ";

            sql += id + ",'" + nombre + "','" + descripcion + "'";
            sql += ",'" + precio + "','" + cuit + "'";

            bd.ejecutar_procedimiento(sql);

        }
        public void baja_Articulo(string id_articulo)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec baja_articulos " + id_articulo;

            bd.ejecutar_procedimiento(sql);
        }
        public DataTable recuperar_datos(string id)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable articulo = new DataTable();

            string sql = "select a.Nombre, a.precioUnitario, a.disponibilidad," +
                " a.IdProvedor as 'Proveedor',a.Descripcion";

            sql += " from Articulos a where a.IdArticulos = " + id;

            articulo = bd.ejecutar_consulta(sql);

            return articulo;
        }
        public DataTable buscar_articulo(string buscar_x)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "select a.IdArticulos, a.Nombre, a.precioUnitario, a.estado,p.Nombre as 'Proveedor',a.Descripcion,a.disponibilidad " +
                "from Articulos a inner join Provedores p on p.CUIT = a.IdProvedor ";

            //Console.WriteLine("Buscar: " + sql + buscar_x);
            return bd.ejecutar_consulta(sql + buscar_x);
        }
    }
}
