﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_Criadero1.Clases;
using System.Data;
using System.Windows.Forms;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_clientes
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        DataTable tabla_clientes = new DataTable();

        public void registrar_cliente(string nombre, string apellido,            
            string n_documento, string tipoDoc,
            string iddireccion, string fechaNac,string telefono, string email)
        {
            
            string sql = "exec registrar_cliente " + n_documento + "," + tipoDoc;
            sql += ",'" + nombre + "','" + apellido + "'," + iddireccion;
            sql += ",'" + fechaNac + "','" + telefono + "','" + email + "'";

            bd.ejecutar_procedimiento(sql);
        }

        public void modificar_cliente(string nombre, string apellido,
            string telefono, string fechaNac,
            string n_documento, string tipoDoc, string iddireccion, string email)
        {
            Console.WriteLine("Parametro: " + tipoDoc);

            string sql = "exec modificar_cliente " + n_documento + "," + tipoDoc;
            sql += ",'" + nombre + "','" + apellido + "'," + iddireccion;
            sql += ",'" + fechaNac + "', '" + telefono +"', '" + email +"'" ;
           
            bd.ejecutar_procedimiento(sql);

        }

        public DataTable recuperar_datos(string tipoDni, string dni)
        {
            string sql = @"SELECT c.DNI, c.TipoDNI, c.Nombre,c.Apellido,d.Calle,d.Nro,d.IdBarrio,c.FechaNac,c.Telefono, c.Email
                    FROM Clientes c
                    inner join Direcciones d on d.IdDireccion = c.IdDireccion
                    WHERE DNI = " + dni.Trim() + " and TipoDNI = " + tipoDni;
            DataTable tabla = new DataTable();
            BE_conexion_criadero_BD _BD = new BE_conexion_criadero_BD();

            tabla = _BD.ejecutar_consulta(sql);
            return tabla;
        }

        public void baja_cliente(string dni_Cliente)
        {
            string sql = "exec baja_cliente " + dni_Cliente;

            bd.ejecutar_procedimiento(sql);
        }
    }
}
