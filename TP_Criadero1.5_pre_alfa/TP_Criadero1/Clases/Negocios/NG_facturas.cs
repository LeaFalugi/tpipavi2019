﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_Criadero1.Clases;
using System.Data;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_facturas
    {
        public string nro_factura { get; set; }
        public string idTipo_factura { get; set; }

        //@tipo_fact int,
        //@dni_emp int,
        //@tipoDoc_emp int,
        //@dni_cli int,
        //@tipoDoc_cli int,
        //@fechaEmi_fact date,
        //@cant_art int,
        //@id_art int
        
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        public void registrar_factura(string idTipoFac, string dniEmp, string tipoDniEmp,
            string dniCli, string tipoDniCli, string fechaEmi)
        {

            string sql = "exec registrar_factura ";

            sql += idTipoFac + "," + dniEmp + "," + tipoDniEmp + "," + dniCli + "," + tipoDniCli + ",";
            sql += "'" + fechaEmi + "'";

            bd.inicio_transaccion();
            bd.ejecutar_procedimiento(sql);
            bd.cerrar_transaccion();
            recuperar_ids_factura();
        }
        public void recuperar_ids_factura()
        {
            DataTable tabla_id = new DataTable();

            tabla_id = bd.ejecutar_consulta("select top(1) nroFactura,idtipofactura from Facturas " +
                "order by NroFactura desc");

            nro_factura = tabla_id.Rows[0]["nroFactura"].ToString();
            idTipo_factura = tabla_id.Rows[0]["idtipofactura"].ToString();

        }
        public void registrar_det_factura(string idArticulo, string cantidad)
        {

            string sql = " exec registrar_det_factura ";

            sql += nro_factura + "," + idTipo_factura + "," + idArticulo + "," + cantidad;

            bd.inicio_transaccion();
            bd.ejecutar_procedimiento(sql);
            bd.cerrar_transaccion();
        }
        public DataTable buscar_facturas(string buscar)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select f.NroFactura, a.Nombre as 'Articulo', " +
                "d.cantidad,c.Nombre as 'nomCliente', c.Apellido as 'apeCliente'," +
                "e.Nombre as 'nomEmpleado', e.Apellido as 'apeEmpleado'," +
                "t.Descripcion as 'tipoFactura',f.FechaEmision";

            sql += " from Facturas f " +
                "inner join Clientes c on (c.DNI=f.DniCliente and c.TipoDNI=f.TipoDniCliente)" +
                " join Empleados e on (e.DNI=f.DniEmpleado and e.TipoDNI=f.TipoDniEmpleado) " +
                "join TiposFacturas t on t.IdTipoFactura=f.IdTipoFactura " +
                "join DetalleFactura d on (d.NroFactura=f.NroFactura and d.IdTipoFactura=f.IdTipoFactura)" +
                " join Articulos a on a.IdArticulos=d.IdArticulo";

            sql += "\n"+ buscar;

            tabla = bd.ejecutar_consulta(sql);
            return tabla;
        }
    }
}
