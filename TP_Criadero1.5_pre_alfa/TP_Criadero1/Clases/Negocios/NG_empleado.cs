﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_Criadero1.Clases;
using System.Data;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_empleado
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

        public void registrar_empleado(int dni, string tipoDni, 
            string nombre, string apellido,
            string fechaNacimiento,
            string telefono, string idDireccion)
        {
            string consulta = "exec registrar_empleado ";
            consulta += dni + "," + tipoDni;
            consulta += ", '" + nombre + "','" + apellido + "'";
            consulta += ",'" + fechaNacimiento + "','" + telefono + "'," + idDireccion;
            Console.WriteLine("exec :" + consulta);
            bd.ejecutar_procedimiento(consulta);
        }

        public void modificar_empleado(string dni_emp, string tipoDni_emp,
            string nombre, string apellido,
            string fechaNacimiento,
            string telefono, string idDireccion)
        {
            string consulta = "exec modificar_empleado ";
            consulta += dni_emp + "," + tipoDni_emp + ",";
            consulta += "'" + nombre + "','" + apellido + "',";
            consulta += idDireccion + ",'" + fechaNacimiento + "','" + telefono+"'";

            bd.ejecutar_procedimiento(consulta);
        }

        public DataTable recuperar_datos(string tipoDni, string dni)
        {
            string sql = @"SELECT e.DNI, t.IdTipoDNI, e.Nombre, e.Apellido,d.Calle,d.Nro,d.IdBarrio,e.FechaNacimiento, e.Telefono
                    FROM Empleados e
                    inner join Direcciones d on d.IdDireccion = e.IdDireccion
					join TiposDNI t on t.IdTipoDNI = e.TipoDNI
                    WHERE e.DNI = " + dni.Trim() + " and t.Descripcion like '" + tipoDni+"'";
            DataTable tabla = new DataTable();
            BE_conexion_criadero_BD _BD = new BE_conexion_criadero_BD();
            tabla = _BD.ejecutar_consulta(sql);
            return tabla;
        }

        public void baja_Empleado(string dni_Empleado)
        {
            string sql = "exec baja_empleado " + dni_Empleado;

            bd.ejecutar_procedimiento(sql);
        }

        public DataTable recuperar_dni_tipo(string nombre_apellido)
        {
            DataTable tabla_empleados = new DataTable();
            string sql = "select DNI, TipoDNI from Empleados where CONCAT(nombre,' ',apellido)='"
                 + nombre_apellido+"'";

            //Console.WriteLine("dni: " + nombre_apellido);
            //Console.WriteLine("Consulta: " + sql);
            tabla_empleados = bd.ejecutar_consulta(sql);
            //Console.WriteLine("tablaempleados: " + tabla_empleados.Rows.Count);
            return tabla_empleados;
        }
    }
}
