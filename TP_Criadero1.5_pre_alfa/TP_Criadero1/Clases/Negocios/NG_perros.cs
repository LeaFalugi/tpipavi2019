﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;
using System.Data;
using System.Windows.Forms;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_perros
    {
        BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
        public int tomar_id_madre(string nombre)
        {
            DataTable madre = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "select IdPerro from Perros where Nombre = '" + nombre + "' and sexo = 'hembra'";

            madre = bd.ejecutar_consulta(sql);

            return int.Parse(madre.Rows[0]["idperro"].ToString());

        }
        public string tomar_nombre_madre(string idMadre)
        {
            DataTable madre = new DataTable();
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "select nombre from Perros " +
                "where IdPerro= " + idMadre + " and sexo='hembra'";

            madre = bd.ejecutar_consulta(sql);

            return madre.Rows[0]["nombre"].ToString();
        }
        
        public DataTable recuperar_datos(string id_perro)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable perro = new DataTable();

            string sql = "select nombre, IdPerroMadre,FechaNacimiento, sexo, estado" +
                " from Perros where IdPerro= " + id_perro;

            perro = bd.ejecutar_consulta(sql);

            return perro;
        }

        public int recuperar_id(string nombre)
        {
            DataTable tabla = new DataTable();

            string sql = "select p.IdPerro " +
                        "from Perros p " +
                        "where p.Nombre = '" + nombre + "'";

            tabla = bd.ejecutar_consulta(sql);

            if (tabla.Rows.Count==0)
            {
                return 0;
            }
            else
            {
                return int.Parse(tabla.Rows[0]["idPerro"].ToString());
            }
            
        }

        public void registrar_perro(string nombre, string fechaNac,
            string sexo, string madre)
        {
            string sql = "exec registrar_perro ";

            sql += "'" + nombre + "','"+fechaNac+"', '" + sexo + "',";

            if (madre == "")
            {
                sql += "null";
            }
            else
            {
                
                sql += tomar_id_madre(madre);
            }

            bd.ejecutar_procedimiento(sql);

        }

        public void modificar_perro(string id, string nombre, string madre,
            string fechaNac, string sexo)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "exec modificar_perro ";

            sql += id + ",'" + nombre + "',";

            if (madre == "")
            {
                sql += "null";
            }
            else
            {

                sql += tomar_id_madre(madre);
            }

            sql += ",'" + fechaNac + "','" + sexo + "'";

            bd.ejecutar_procedimiento(sql);

        }

        public void baja_Perro(string id_Perro)
        {
            string sql = "exec baja_perro " + id_Perro;

            bd.ejecutar_procedimiento(sql);
        }

        
    }
}
