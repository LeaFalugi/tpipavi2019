﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using TP_Criadero1.Clases;
using TP_Criadero1.Clases.Negocios;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_eventos
    {
        
        
        public DataTable recuperar_datos(string id)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select nombre, fecha, descripcion, idciudad from Eventos where IdEvento=" + id;

            tabla = bd.ejecutar_consulta(sql);

            return tabla;
        }
        public void registrar_evento(string nombre, string fecha,
            string descripcion, string idCiudad)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_evento ";

            sql += "'" + nombre + "','" + fecha + "','" + descripcion + "'";
            sql += "," + idCiudad;

            bd.ejecutar_procedimiento(sql);
        }
        public void modificar_evento(string id, string nombre, string fecha,
            string descripcion, string idciudad)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_evento ";

            sql += id + ",'" + nombre + "','" + fecha + "','" + descripcion + "'," + idciudad;

            bd.ejecutar_procedimiento(sql);
        }
        public void baja_Evento(string id_Evento)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            string sql = "exec baja_eventos " + id_Evento;

            bd.ejecutar_procedimiento(sql);
        }
    }
}
