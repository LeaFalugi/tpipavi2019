﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_proveedores
    {
        //PK: cuit
        public DataTable recuperar_datos(string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select Nombre, Telefono, Mail, IdDireccion, estado " +
                "from Provedores where CUIT='" + cuit + "'";

            tabla = bd.ejecutar_consulta(sql);

            return tabla;
        }

        public string recuperar_cuit(string nombre, string telefono)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "select CUIT "+
                "from Provedores "+
                "where Nombre = '" + nombre + "' " +
                " and Telefono = " + telefono;

            DataTable tabla = new DataTable();

            tabla = bd.ejecutar_consulta(sql);

            return tabla.Rows[0]["cuit"].ToString();
        }

        public void registrar_proveedor(string cuit, string nombre,
            string telefono, string idDireccion, string mail)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_proveedor ";

            sql += "'" + cuit + "' , '" + nombre + "','";
            sql += telefono + "','" + mail + "'," + idDireccion;


            bd.ejecutar_procedimiento(sql);
        }
        public void modificar_proveedor(string cuit,string nombre,string telefono,
            string mail,string id_dir)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_proveedor ";

            sql += "'" + cuit + "','" + nombre + "','" + telefono + "','" + mail + "'," + id_dir;

            bd.ejecutar_procedimiento(sql);
        }
        public void baja_Proveedor(string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec baja_proveedor '" + cuit + "'";

            bd.ejecutar_procedimiento(sql);
        }
    }
}
