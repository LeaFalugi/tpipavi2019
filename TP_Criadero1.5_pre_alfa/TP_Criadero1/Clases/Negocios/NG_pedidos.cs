﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TP_Criadero1.Clases.Negocios
{
    class NG_pedidos
    {
        public void transaccion_pedido(string nro_pedido, string cuit, string fechaEfec)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            
            string sql = "exec baja_pedido " + nro_pedido + ",'" + cuit + "'";

            sql += "\n" + "update Pedidos set fechaEfectiva= '" + fechaEfec +
                "' where nro= " + nro_pedido + " and id_provedor='" + cuit + "'";
            Console.WriteLine("sql: " + sql);
            bd.inicio_transaccion();
            bd.ejecutar_procedimiento(sql);
            bd.cerrar_transaccion();
        }
        public DataTable recuperar_datos(string nro, string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();

            string sql = "select * from pedidos where nro=" + nro + " and id_provedor='" + cuit + "'";
            //Console.WriteLine("Recuperar datos- pedido: " + sql);
            tabla = bd.ejecutar_consulta(sql);

            return tabla;
        }
        public DataTable pedidos_x_proveedor(string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();
            DataTable tabla = new DataTable();
            string sql = "select * from Pedidos where id_provedor='" + cuit + "'  and estado is null";

            Console.WriteLine("pedidos x proveedor: " + sql);
            tabla = bd.ejecutar_consulta(sql);
            return tabla;
        }
        public void registrar_pedido(string cuitProv, string fechaEmision, string fechaEfec)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec registrar_pedido '";

            sql += cuitProv.Trim() + "','" + fechaEmision.Trim() + "',";


            if (fechaEfec == null)
            {
                sql += "null";
            }
            else
            {
                sql += "'" + fechaEfec + "'";
            }

            bd.ejecutar_procedimiento(sql);
        }

        public int id_ultimo_pedido_registrado()
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            DataTable tabla_id = new DataTable();
            tabla_id = bd.ejecutar_consulta("select top(1) nro from Pedidos order by nro desc");

            return int.Parse(tabla_id.Rows[0]["nro"].ToString());
        }
        public void modificar_pedido(string nro_ped,string cuit, string fechaEmi, string fechaEfec)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec modificar_pedido ";

            sql += nro_ped + ",'" + cuit + "','" + fechaEmi + "','" + fechaEfec + "'";
            Console.WriteLine("Modif_pedido: " + sql);

            bd.ejecutar_procedimiento(sql);

        }
        public void baja_Pedido (string nro_Pedido,string cuit)
        {
            BE_conexion_criadero_BD bd = new BE_conexion_criadero_BD();

            string sql = "exec baja_pedido " + nro_Pedido + ",'" + cuit + "'";

            bd.inicio_transaccion();
            bd.ejecutar_procedimiento(sql);
            bd.cerrar_transaccion();

        }
    }
}
