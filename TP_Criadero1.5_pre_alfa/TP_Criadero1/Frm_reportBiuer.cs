﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Criadero1
{
    public partial class Frm_reportBiuer : Form
    {
        public Frm_reportBiuer()
        {
            InitializeComponent();
        }

        private void Frm_reportBiuer_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'Conjuntos_Criader.ventas_x_mes' Puede moverla o quitarla según sea necesario.
            this.ventas_x_mesTableAdapter.Fill(this.Conjuntos_Criader.ventas_x_mes);

            this.reportViewer1.RefreshReport();
        }
    }
}
