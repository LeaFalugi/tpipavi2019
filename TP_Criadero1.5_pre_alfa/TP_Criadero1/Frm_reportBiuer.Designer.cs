﻿namespace TP_Criadero1
{
    partial class Frm_reportBiuer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.ventas_x_mesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Conjuntos_Criader = new TP_Criadero1.Conjuntos_Criader();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ventas_x_mesTableAdapter = new TP_Criadero1.Conjuntos_CriaderTableAdapters.ventas_x_mesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ventas_x_mesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conjuntos_Criader)).BeginInit();
            this.SuspendLayout();
            // 
            // ventas_x_mesBindingSource
            // 
            this.ventas_x_mesBindingSource.DataMember = "ventas_x_mes";
            this.ventas_x_mesBindingSource.DataSource = this.Conjuntos_Criader;
            // 
            // Conjuntos_Criader
            // 
            this.Conjuntos_Criader.DataSetName = "Conjuntos_Criader";
            this.Conjuntos_Criader.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.AutoSize = true;
            this.reportViewer1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.DocumentMapWidth = 1;
            reportDataSource1.Name = "DataSet1_ventas_x_mes";
            reportDataSource1.Value = this.ventas_x_mesBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "TP_Criadero1.ReporteVentas.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(798, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // ventas_x_mesTableAdapter
            // 
            this.ventas_x_mesTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_reportBiuer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 450);
            this.Controls.Add(this.reportViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Frm_reportBiuer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_reportBiuer";
            this.Load += new System.EventHandler(this.Frm_reportBiuer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ventas_x_mesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conjuntos_Criader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource ventas_x_mesBindingSource;
        private Conjuntos_Criader Conjuntos_Criader;
        private Conjuntos_CriaderTableAdapters.ventas_x_mesTableAdapter ventas_x_mesTableAdapter;
    }
}