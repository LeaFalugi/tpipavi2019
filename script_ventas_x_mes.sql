USE [Criadero]
GO
/****** Object:  StoredProcedure [dbo].[ventas_x_mes]    Script Date: 28/10/2019 21:43:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ventas_x_mes]
as
select dt.NroFactura, tf.Descripcion,f.FechaEmision, a.Nombre, dt.cantidad, 
(dt.Precio*dt.cantidad)as total
from DetalleFactura dt
join TiposFacturas tf on tf.IdTipoFactura=dt.IdTipoFactura
join Articulos a on a.IdArticulos=dt.IdArticulo
join Facturas f on (f.NroFactura=dt.NroFactura and f.IdTipoFactura=dt.IdTipoFactura)
order by MONTH(f.FechaEmision) desc
GO
