USE [Criadero]
GO
/****** Object:  StoredProcedure [dbo].[contador_fact_x_mes]    Script Date: 28/10/2019 23:10:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[contador_fact_x_mes]
as
select COUNT(*) as contador, MONTH(FechaEmision) as mes
from Facturas
group by MONTH(FechaEmision)
order by MONTH(fechaEmision)
GO
